from shopify_admin.models import ShopifyAccessToken 
from shopify_admin.views import ShopifyLogin
from rest_framework.response import Response
from .Serializer import UserSerializer,SignSerializer,VariantSerializer,ShopifyTokens
from django.contrib.auth.models import User,Group
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from shop.models import Variant,Products
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse
    
class auth(APIView):
    
    
    permission_classes = (IsAuthenticated,)
    print(permission_classes)
    def getgroup(Self,request):
        if request.user in Group.objects.get(name="manufacturer").user_set.all():
            
            group1 = "manufacturer"
        elif request.user.is_superuser:
            
            group1 = "superuser"
        else:
            
            group1 = "warehouse"
        return group1
    def get(self,request):
        group = self.getgroup(request)
        user = User.objects.get(username=request.user)
        print(user)
        serializer= UserSerializer({'Groups':group,'username':user.username,'email':user.email,'firstname':user.first_name,'lastname':user.last_name})
        
        return Response(serializer.data)
    

class Signup(APIView):
    
    
    def post(self,request):
        data=request.data
        
        if(data['password1']==data['password2']):
            
            serializer=SignSerializer(data=request.data)
            if(serializer.is_valid()):
                print(serializer)
                serializer.save(data)

        return Response(serializer.errors)

class Product_list(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request):
        queryset = Variant.objects.all()
        serializer = VariantSerializer(queryset,many=True)
        return Response(serializer.data)
class get_by_count(APIView):
    def get(self,request,*args,**kwargs):
        rate = kwargs.get('rate', 'Default Value if not there')
        if(rate=='medium'):
            queryset = Variant.objects.filter(inventory_item_quantity__lte=99).filter(inventory_item_quantity__gte=20)
            print(queryset)
        elif(rate=='available'):
            queryset = Variant.objects.filter(inventory_item_quantity__gte=100)
        elif(rate=='end'):
            queryset = Variant.objects.filter(inventory_item_quantity__lte=19)
            print(queryset)
        serializer = VariantSerializer(queryset,many=True)
        return Response(serializer.data)
        
class getshopify(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self,request):
        data = request.data

        print(data['store'])
        api_key = '89d63250984d1ea3d1ea5f1fc7b920c1'
        api_secret_key = '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'
    # api_key = 'ead441fdcf80b836985b08647f6119c0'
    # api_secret_key= '8e243f0dd108f7402056831f5d72c22d'
        scope =str(['read_products'])    
        response = "https://"+data['store']+"/admin/oauth/authorize?client_id="+api_key +'&scope=write_products' +'&redirect_uri='+'http://127.0.0.1:8000/shopify_app/catch/'+'&state=12345'
        print(response)
        return HttpResponseRedirect(response)
    def get(self,request):
        print(request.user.username)
        queryset = ShopifyAccessToken.objects.all().filter(user_name=request.user.username)
        for a in queryset:
           queryset=a
        
        serilizer= ShopifyTokens(queryset)
        print(serilizer.data)
        return Response(serilizer.data)
