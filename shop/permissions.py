from rolepermissions.permissions import register_object_checker
from core.roles import SystemAdmin

@register_object_checker()
def access_order(role, user, order):
    if role == SystemAdmin:
        return True

    if user.order == order:
        return True

    return False