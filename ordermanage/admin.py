from django.contrib import admin
from .models import *


'''==========================================================================
    Project Name: OrderManage
    Name: Display MOdelField in Admin
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''
admin.site.site_header ="eKnous Technology Solutions"
admin.site.site_title = "OrderMange"
admin.site.index_title= "Order Mange"

'''=====================================
   =======ORDERMANAGE DISPLAY TABLE=====
   ====================================='''

class OrderAdmin(admin.ModelAdmin):
    model = Order
    fields = ["user","po","name","company_name","courier_method","Directory_Name","order_po_date" ,"status","invoice_send","remark", "flag", "traking_number"]
    search_fields = ('po', "company_name", "invoice_send")   
    list_display =["po","name","company_name","courier_method","Directory_Name","order_date" ,"status","invoice_send","remark"]
admin.site.register(Order, OrderAdmin)

'''=====================================
   =========COURIER DISPLAY TABLE=======
   ====================================='''

class CourierMethodAdmin(admin.ModelAdmin):
    list_display = ["courier_method","valid_code"]   
admin.site.register(CourierMethod, CourierMethodAdmin)

'''=====================================
   =======ORDER SKU DISPLAY TABLE=======
   ====================================='''
class LineTableAdmin(admin.ModelAdmin):
    list_display = ['purchase_order','vendor_SKU','order_quantity']
admin.site.register(LineTable, LineTableAdmin)

'''======================================
   ====WAREHOUSE REMARK DISPLAY TABLE====
   ======================================'''

class remarkAdmin(admin.ModelAdmin):
    list_display= ['remark_po', 'user', 'comment']
admin.site.register(remark, remarkAdmin)

class GetOrderAdmin(admin.ModelAdmin):
    model = GetOrder
    list_display= ['customer_id', 'customer_name', 'contact_number']
admin.site.register(GetOrder, GetOrderAdmin)

class ShopifyAccessTokenAdmin(admin.ModelAdmin):
    model = ShopifyAccessToken
    list_display= ['shopify_shop','access_token', 'scope']

admin.site.register(ShopifyAccessToken, ShopifyAccessTokenAdmin)

'''=====================================================
===================Addres Table=========================
======================================================'''

class AddressAdmin(admin.ModelAdmin):
    model = Address
    list_display= ['po_number','address','city','state','zip','phone_number']

admin.site.register(Address, AddressAdmin)


