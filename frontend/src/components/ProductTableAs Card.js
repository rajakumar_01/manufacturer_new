import React, { Component } from 'react'
import { Container } from 'react-bootstrap'

class ProductTable extends Component{
    constructor(props){
        super(props)
        this.state={
            content:[]
        }
        console.log(this.props)
        
    }
    componentDidMount(){
        const token =  "Token "+this.props.token
        const requestOptions = {
            method: 'GET',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': token,
            },
            // body: JSON.stringify({data:this.props.location.state.token})    
            
        }
        switch (this.props.rate) {
            case 'medium':
                url=process.env.URL+'api/product_list/medium/'
                break;
            case 'end':
                url=process.env.URL+'api/product_list/end/'
                break;
            case 'available':
                url=process.env.URL+'api/product_list/available/'
                break;
            default:
                break;
        }
        fetch(url, requestOptions)
            .then(response => response.json())
            .then(data => this.setState({content:data}))    
    }

    render(){
        console.log(this.state)
        
        const tableContent = this.state.content.map(Variant => {
            return (
               <tr key={Variant.id}>
                   <th>{Variant.variant_sku}</th>
                   <th>{Variant.variant_image===null?<img className="variantlogo" src={logo}/>:<img src={Variant.variant_image}/>}</th>
                   <th>{Variant.product_id.title}</th>
                   <th>{Variant.title}</th>
                   {console.log(Variant.product_id.title)}
                   <th>{Variant.created.slice(0,10)}</th>
                   <th>{Variant.updated.slice(0,10)}</th>
                   <th>{Variant.inventory_item_quantity}</th>
                   <th>Button</th>
               </tr>
           )
       })
       
        return(
            <div className="ProductCotainer">
                <table>
                    <thead>
                    <tr>
                        <th>Variant SKU</th>
                        <th>Image</th>
                        <th>Product Name</th>
                        <th>Variant Name</th>
                        <th>Created Date</th>
                        <th>Updated Date</th>
                        <th>Available Quantity</th>
                        <th>Button</th>
                    </tr>
                    </thead>
                    <tbody>
                    {tableContent}
                    </tbody>
                </table>    
                
            </div>
        )
    }
}



export default ProductTable