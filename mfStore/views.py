# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from io import BytesIO
from core import settings
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.contrib.auth.models import User, Permission
from django.core.mail import EmailMessage
from django.core.mail import send_mail
from ordermanage.views import chek_group
import core.settings
from .models import *
from .forms import *
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import Group
import xml.etree.ElementTree as ET
import datetime
from django.template.loader import get_template
from xhtml2pdf import pisa
from django.views.generic import View
from django.template import Context
import pdfkit
from reportlab.pdfgen import canvas
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from core.tokens import account_activation_token
from django.core.mail import EmailMessage
# import barcode
import random
# Create your views here.

"""===================================="""
""">>>>>>> Add New Manufacturer >>>>>>>"""
"""===================================="""


# @login_required(login_url="/")
# def add_manufacturer(request):
#     group1 = chek_group(request.user)
#     query_results = AddManufacturer.objects.all()
#     if request.method == "POST":
#         form = AddManufacturerForm(request.POST or None)
#         if form.is_valid():
#             instance = form.save()
#             user = User.objects.get(
#                 username=instance.manufacturer_name)
#             user.last_login = datetime.date.today()
#             user.save()
#             instance.save()
#             messages.success(request, "Regitration successfull")

#     else:
#         form = AddManufacturerForm()
#     context = {'group1': group1,

#                'form': form,
#                'query_results': query_results

#                }
#     return render(request, "mfStore/add_manufacturer.html", context)

@login_required(login_url="/")
def add_manufacturer(request):
    group1 = chek_group(request.user)
    query_results = AddManufacturer.objects.all()
    print (query_results)
    if request.method == "POST":
        form = AddManufacturerForm(request.POST or None)
        # print form
        # user = User
        if form.is_valid():
            instance = form.save(commit=False)
            
            instance.manufacturer_name = instance.manufacturer_name.replace(
                " ", "")  # removing whitespace
            count = AddManufacturer.objects.filter(
                manufacturer_name=instance.manufacturer_name).count()
            # print count
            if count == 0:
                
                user_instance = User.objects.create_user(username=instance.manufacturer_name,
                                                         password=form.cleaned_data['password'], email=form.cleaned_data['email'])
                group = Group.objects.get(name='manufacturer')
                user_instance.groups.add(group) 
                print (group)
                user_instance.save() 
                instance.save()
                
                mail_subject = 'Activate your OrderMange UserAccount.'
                message = render_to_string('mfStore/activate_account.html', {
                    'user': user_instance,
                    'domain': 'http://127.0.0.1:8000',
                    'uid': urlsafe_base64_encode(force_bytes(user_instance.pk)),
                    'token': account_activation_token.make_token(user_instance),                    
                })
                to_email = form.cleaned_data.get('email')
                email = EmailMessage(
                    mail_subject, message, to=[to_email]
                )
                email.send()
                
                messages.success(request, "Ask Manufacturer to Activate ")
                # return HttpResponse('<h1>Please confirm your email address to complete the registration</h2>')
            # return redirect('mvp',)
            else:
                messages.success(request, "Manufacturer is already registerd")
    else:
        form = AddManufacturerForm()
    return render(request, "mfStore/add_manufacturer.html", {'form': form, 'query_results': query_results, 'group1': group1})



"""===================================="""
""">>>>> Map Manufacturer With Sku >>>>>"""
"""===================================="""


@login_required(login_url="/")
def map_manufacturer_sku(request):
    group1 = chek_group(request.user)
    manufacrtrer_mapping= ManufacturerMapping.objects.all()
    
    if request.method == "POST":        
        form = ManufacturerMappingForm(request.POST or None)
        if form.is_valid():
            instance = form.save(commit=False)
            already_mapped = ManufacturerMapping.objects.filter(
                Product_SKU=instance.Product_SKU).filter(manufacturer=instance.manufacturer)
            if not already_mapped.exists():
                instance.save()
                messages.success(request, "Successfully Maped")
            else:
                messages.success(request, "Already Maped with same sku")
    else:
        form = ManufacturerMappingForm()
    return render(request, "mfStore/map_manufacturer.html", {'form': form, 'group1': group1, 'manufacturer_mapping': manufacrtrer_mapping})


"""============================================"""
""">>>>>>>>>> Create A Order Ewquest >>>>>>>>>>"""
"""============================================"""


@login_required(login_url="/")
def create_order(request):
    group1 = chek_group(request.user)
    queryset_shop = Company.objects.all()
    if request.method == "POST":
        print ('this is ', request.user)
        form = OrderRequestForm(request.POST or None)
        formItem = LineItemForm(request.POST or None)
        mfSku_list1 = len(request.POST.getlist("new_Sku1"))
        print(mfSku_list1)
        if mfSku_list1 > 0:
            if form.is_valid():
                instance = form.save(commit=False)
                instance.order_by = request.user.username
                instance.save()
                address_instance = Address
                address_save_query = address_instance(
                    manufacturer_name=CreateManufacturerOrder.objects.get(
                        id=instance.id),
                    address=request.POST.get('address'),
                    city=request.POST.get('city'),
                    state=request.POST.get('state'),
                    zip=request.POST.get('zip'),
                    contact_number=request.POST.get('contact_number'))
                address_save_query.save()
                print ('add email==============')
                print(instance.manufacturer_list.manufacturer_name)
                email = User.objects.get(
                    username=instance.manufacturer_list.manufacturer_name).email
                print ("email:",email)
                reciveremail = email 
                message = "order is placed successfully need to acknoledge"
                emailFunction(sender=request.user.email,reciver=reciveremail , message=message)
                remark_tbl = remark
                comment = request.POST.get("remark")
                print (request.user)
                if not comment.isspace() and comment:
                    remark_comment = remark_tbl(
                        wh_remark=CreateManufacturerOrder.objects.get(id=instance.id), comment=comment, comment_by=request.user)
                    remark_comment.save()
                limeTbl_object = ManufactureLineTable
                mfSku_list = request.POST.getlist("new_Sku1")
                mf_quantity = request.POST.getlist("qty")
                mf_packsize = request.POST.getlist("pack_size")
                mf_orderdate = request.POST.getlist("calender")
                print (mf_orderdate)
                for i, j, k, p in zip(range(len(mfSku_list)), range(len(mf_quantity)), range(len(mf_orderdate)), range(len(mf_packsize))):
                    mf_sku = mfSku_list[i]
                    print ("============", mf_sku)
                    quentity = mf_quantity[j]
                    order_date = mf_orderdate[k]
                    pack_size = mf_packsize[p]
                    product_object = (ManufacturerMapping.objects.filter(
                        Manufacturer_SKU=mf_sku)[0]).Product_SKU
                    # print (ManufacturerMapping.objects.filter(
                    #     Manufacturer_SKU=mf_sku)[0]).Manufacturer_SKU
                    print ("this tyoe of qty" ,type(quentity))
                    if (int(quentity) % int(pack_size)) == 0:                    
                        save_mfSku = limeTbl_object(manufacturer_table=CreateManufacturerOrder.objects.get(
                            id=instance.id),
                            item_sku=ManufacturerMapping.objects.filter(Manufacturer_SKU=mf_sku)[0],
                            item_name=product_object,
                            dimention=product_object.weight,
                            color=product_object,
                            quantity=quentity,
                            pack_size=pack_size,
                            delivery_date=order_date,)
                    
                        messages.success(request, "Successfully Created")
                        save_mfSku.save()
                    else:
                        save_mfSku = limeTbl_object(manufacturer_table=CreateManufacturerOrder.objects.get(
                            id=instance.id),
                            item_sku=ManufacturerMapping.objects.filter(
                                Manufacturer_SKU=mf_sku)[0],
                            item_name=product_object,
                            dimention=product_object.weight,
                            color=product_object,
                            quantity=quentity,
                            pack_size=pack_size,
                            delivery_date=order_date,)
                        messages.success(request, "Quantity is not divisible by Packsize please change")
                        save_mfSku.save()
        else:
            messages.error(request, "Please Add Line item")

    context = {
        "form": OrderRequestForm(),
        "formItem": LineItemForm(request.POST or None),
        "ManufactureLineTable": ManufacturerMapping.objects.all(),
        'group1': group1,
        'queryset_shop' : queryset_shop,


    }
    return render(request, "mfStore/create.html", context)


"""===================================="""
""">>>>>>>>>> Add New Manufacturer >>>>>>>>>>"""
"""===================================="""


@login_required(login_url="/")
def linetblcreate(request):
    if request.method == "POST":
        form = LineItemForm(request.POST or None)
        print (form)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.manufacturer_table = CreateManufacturerOrder.objects.get(
                manufacturer_list_id=1)
            instance.save()
            messages.success(request, "Successfully Added")
        else:
            print ("form is not valide")
    return render(request, "mfStore/test.html", {'form': LineItemForm()})


"""=========================================="""
""">>>> Add New Manufacturer Order list >>>>"""
"""=========================================="""

@login_required(login_url="/")
def ManufacturerOrderlist(request):
    group1 = chek_group(request.user)
    address_instance = AddManufacturer.objects.filter(
        manufacturer_name=request.user)    
    notification = remark.objects.all().order_by("-date")
    # notification = remark.objects.all().order_by("-date")
    lineitem  = ""
    notification = ''
    warehouse_status = ""
    if group1 == "manufacturer":
        try:
            commment_query = remark.objects.get(comment_by = request.user)
            print ("this is comment_query",commment_query)
            print ("this is comment_query2", commment_query.wh_remark)
        
            query = remark.objects.filter(wh_remark=commment_query.wh_remark)

            notification = query
        except:
            print ("nodata")
        print(address_instance)
        # print(address_instance.manufacturer_name)
        query_results_whorder = CreateManufacturerOrder.objects.filter(
            manufacturer_list__in=address_instance).order_by("-order_date")
        print("strig",query_results_whorder)
        total_open = CreateManufacturerOrder.objects.filter(
            manufacturer_list__in=address_instance, order_status="open").count()
        close_order = CreateManufacturerOrder.objects.filter(
            manufacturer_list__in=address_instance, order_status="close").count()
        print ("total_open", total_open)
        for queryset in query_results_whorder:
            lineitemstatus = ManufactureLineTable.objects.filter(
                manufacturer_table=queryset.id)            
            for line in lineitemstatus:
                lineitem = line.mf_status
                warehouse_status = line.warehouse_status
    else:
        notification = remark.objects.all().order_by("-date")
        query_results_whorder = CreateManufacturerOrder.objects.all().filter(order_by=request.user.username).order_by("-order_date")
        print()
        print(request.user.username)
        total_open = CreateManufacturerOrder.objects.filter( order_status="open").count()
        close_order = CreateManufacturerOrder.objects.filter(
            order_status="close").count()
        for queryset in query_results_whorder:
            lineitemstatus = ManufactureLineTable.objects.filter(
                manufacturer_table=queryset.id)            
            for line in lineitemstatus:
                lineitem = line.mf_status
                warehouse_status = line.warehouse_status
        print (query_results_whorder)
    print(lineitem)
    context = {'query_results_whorder': query_results_whorder,
                'group1': group1, 'notification': notification,
                'group1': group1, 
                'notification': notification,
                'lineitem': lineitem,
                "total_open": total_open,
                "close_order": close_order,
                "warehouse_status": warehouse_status
            }
    return render(request, "mfStore/orderlist.html", context )


"""=========================================="""
""">>>> Add New Manufacturer Order Details >>>>"""
"""=========================================="""

"""=========================================="""
""">>>> Add New Manufacturer Order Details >>>>"""
"""=========================================="""

@login_required(login_url="/")
def order_details(request, id=None):
    group1 = chek_group(request.user)
    order_details = get_object_or_404(CreateManufacturerOrder, id=id)
    print (order_details.id,'aaaaaa')
    print('a')
    line_query = ManufactureLineTable.objects.filter(
        manufacturer_table=order_details.id)
    # a = generate_view(line_query)
    # print a
    print(line_query)
    print('b')
    address_query = Address.objects.get(manufacturer_name=order_details.id)
    remark_query = remark.objects.filter(wh_remark=order_details.id)
    order_position = int(order_details.step_position)
    remark_tbl = remark
    print('c')
    if request.method == "POST":
        comment = request.POST.get("remark")
        if not comment.isspace() and comment:
            remark_comment = remark_tbl(wh_remark=CreateManufacturerOrder.objects.get(
                id=id), comment=comment, comment_by=request.user)
            remark_comment.save()
            messages.success(request, "Successfully Added Remark")
    print("redirecting")
    return render(request, "mfStore/details.html", {'order_details': order_details, 'group1': group1, 'remark_query': remark_query, 'line_query': line_query, 'address_query': address_query, "order_position": order_position})


"""=========================================="""
""">>>> Add New Manufacturer Order Edit >>>>"""
"""=========================================="""

@login_required(login_url="/")
def OrderEdit(request, id=None):
    group1 = chek_group(request.user)
    print("group1==============", group1)
    order_edit = get_object_or_404(CreateManufacturerOrder, id=id)
    address_query = Address.objects.filter(manufacturer_name=order_edit)[0]
    form = OrderRequestForm(request.POST or None, instance=order_edit)
    aa = ManufactureLineTable.objects.filter(
        manufacturer_table=order_edit)
    j = 0
    
    if request.method == 'POST':
        # dd = request.POST.get('id')
        print ("this is submit request", request.POST.get('action'))
        
        if form.is_valid():
            instance = form.save(commit=False)
            
            for a in aa:
                print (a.mf_status)
                print ("this id table", a.step_position)
                step_position = int(a.step_position)
                print ("this is step position", step_position)
                if step_position > j:
                    j = step_position
                    x = j
                    print ("this is x ", x)
                    if "4" in str(x):
                        instance.save()
                        bc = get_object_or_404(ManufactureLineTable, id=a.id)
                        bc.step_position = 5
                        instance.step_position =5
                        bc.save()
                        email = User.objects.get(
                            username=instance.manufacturer_list).email
                        print (email)
                        message = "Order is closed please generate Invoice"
                        emailFunction(sender=request.user.email,reciver=email ,message=message)
                        Address.objects.filter(manufacturer_name=order_edit).update(address=request.POST.get('address'),
                        city=request.POST.get('city'),
                        state=request.POST.get('state'),
                        zip=request.POST.get('zip'),
                        contact_number=request.POST.get('contact_number'))
                        return HttpResponseRedirect(reverse('mf_list'))
                    else:
                        messages.success(
                            request, "All Items are not shiped")
        elif 'download_slip' in request.POST.get('action'):
            print ("this is packging slip request")
        else:
            print ("none of working "   )
    return render(request, "mfStore/edit.html", {'form': form, 'bb': aa, 'address': address_query, 'group1': group1})


"""=========================================="""
""">>>> Liner Item Edit >>>>"""
"""=========================================="""

def lineItemEdit(request, id=None):
    group1 = chek_group(request.user)
    line_edit = get_object_or_404(ManufactureLineTable, id=id)
    ar_id = line_edit.manufacturer_table.id
    if 'download_slip' in request.POST:
        print ("this is packging slip request")
    abc=''
    if request.method == 'POST':        
        if 'save' in request.POST:
            if "Pending" in line_edit.mf_status:
                if(line_edit.quantity == request.POST.get('quantity')):
                    line_edit.mf_status = "Acknowledge"
                    line_edit.warehouse_status = "Acknowledge"
                    if(group1=="manufacturer"):
                        message = "manufacturing process started"
                        to=User.objects.get(username=line_edit.manufacturer_table.order_by).email
                    else:
                        message = "warehouse acknowledge. please start manufacturing"
                        to=User.objects.get(username=line_edit.manufacturer_table.manufacturer_list).email
                    emailFunction(sender=request.user.email,reciver=to,message=message)
                else:
                    message = "quantity updated please Acknowledge"
                    to=User.objects.get(username=line_edit.manufacturer_table.order_by).email
                    emailFunction(sender=request.user.email,reciver=to,message=message)
                    line_edit.warehouse_status = "Pending"
                    line_edit.mf_status = "Pending"
                line_edit.quantity = request.POST.get('quantity')
                line_edit.pack_size = request.POST.get('pack_size')
                print("4444444444444444444444444444444444444444444444444444444444444444444444444444444444444",line_edit.Eta)
                if(line_edit.Eta == None):
                    line_edit.Eta = request.POST.get('eta')

                print (type(line_edit.pack_size))
                line_edit.save()            
                return HttpResponseRedirect(reverse('edit', args=[ar_id]))
            else:
                messages.success(request, "Manufacturer Status is Acknowledged")
        elif 'download_slip' in request.POST:
            abc = GeneratepackingSlip(request)
            print (abc)
            return HttpResponseRedirect(reverse('edit', args=[ar_id]))

        else:
            print ("none of working ")
        
    return render(request, "mfStore/edit.html", {'group1': group1, 'abc': abc})


"""=========================================="""
""">>>>>> send email with 2 parameters >>>>>>"""
"""=========================================="""

"""=========================================="""
""">>>>>> send email with 2 parameters >>>>>>"""
"""=========================================="""

def emailFunction(sender, reciver, message):
    msg = message
    # send_mail('manufacturer Subject', 'Hi We you have new Order From eKnous Warehouse', 'shivam@eknous.com', [
    #     'anuj@eknous.com', email], fail_silently=False)
    send_mail('manufacturer Subject', msg,
              sender, [reciver], fail_silently=False)

    return HttpResponse('success')


"""======================================================"""
""">>>>>> Shiping position of individual line item >>>>>>"""
"""======================================================"""

def shipPosition(request):
    form = WorkProgressBarForm(request.POST or None)
    if request.method == 'POST':
        instance = form.save(commit=False)
        instance.step_id = instance.step_position
        instance.save()

    return render(request, "mfStore/stepprogressbar.html", {"form": form})


"""=============================================================="""
""">>>>>> Shiping position details of individual line item >>>>>>"""
"""=============================================================="""

def shipPositionDetails(request, id=None):
    line_edit = get_object_or_404(WorkProgressBar, id=id)
    order_position = WorkProgressBar.objects.get(id=line_edit.id)
    return render(request, "mfStore/stepprogressbar.html", {"order_position": order_position})


"""=============================================================="""
""">>>>>> For Testing >>>>>>"""
"""=============================================================="""

def shipPositionGraphDetails(id):
    order_position = CreateManufacturerOrder.objects.get(id=id).step_position
    return order_position


"""=============================================================="""
""">>>>>> Details of line item >>>>>>"""
"""=============================================================="""

def lineitemDetails(request, id=None):
    group1 = chek_group(request.user)
    order_details = ManufactureLineTable.objects.get(
        id=id)
    print (int(order_details.step_position))
    return render(request, "mfStore/lineitem.html", {"order_position": int(order_details.step_position), 'order_details': order_details, 'group1': group1})


"""=============================================================="""
""">>>>>> Details of line item >>>>>>"""
"""=============================================================="""


def EditlineitemDetails(request, id=None):
    line_edit = get_object_or_404(ManufactureLineTable, id=id)
    befor_position= int(line_edit.step_position)
    # print line_edit.manufacturer_table
    update_create_orderposition = CreateManufacturerOrder.objects.get(
        invoice_no=line_edit.manufacturer_table)
    status = update_create_orderposition.order_status
    position = update_create_orderposition.step_position
    # print type(position)
    form = MfStatus(request.POST or None, instance=line_edit)
    form_description = WorkProgressBarForm(request.POST or None)
    x = line_edit.id
    workprog = WorkProgressBar.objects.filter(lineitem=x)
    # print('xxxxxxxxxxxxxx', workprog)
    for w in workprog:
        lineit = w.lineitem
        print("lineitem", lineit)
    Steps_query = WorkProgressBar.objects.filter(lineitem=x)
    for i in Steps_query:
        print ("new instance", i.step_position)
   
    if request.method == 'POST':
        instance = form.save(commit=False)        
        after_position = int(instance.step_position)
        print ("this is addition of ordere position ", int(instance.step_position)+1)
        if (("open" in status) and (instance.step_position != '5')):
            if after_position == befor_position+1:
                instance.step_id = instance.step_position
                print("this is line item position ", instance.step_position)
                update_create_orderposition.step_position = instance.step_position
                update_create_orderposition.save()
                instance2 = form_description.save(commit=False)
                instance2.lineitem_id = line_edit.id
                instance2.step_id = instance.step_position
                instance2.step_position = instance.step_position
                instance.save()
                instance2.save()
                # print("###############################################",update_create_orderposition.order_by)
                Stepprogress = (
                                ('1', 'Out-from-MFStore'),
                                ("2", "Dispatch for warehouse"),
                                ("3", "On The Ship"),
                                ("4", "Reached Destination"),
                                ("5", "Deliverd")
                            )
                print("step_position",instance2.step_position)
                progress = Stepprogress[int(instance2.step_position)-1][1]
                print("#############################################################################",progress)
                emailFunction(sender=request.user.email, reciver=User.objects.get(username=update_create_orderposition.order_by).email ,message=progress)
                messages.success(request, "SuccessfullyUpdate Update ")
            else:
                messages.success(request, "please proceed step by step ")
        else:
            messages.success(request, "Please Wait for WareHouse confirmation")

        return HttpResponseRedirect(reverse('mf_list'))
    context = {"form": form,
               "form_description": form_description,
               "order_position": int(line_edit.step_position),
               "Steps_query": Steps_query}
    return render(request, "mfStore/editwp.html", context)


"""==============================================
===============for Testing purpose ==============
=============================================="""


def ReadXmlData(request):
    from xml.dom import minidom
    xml_data = '''<?xml version="1.0" encoding="UTF-8"?>
                <note>
                <to>Tove</to>
                <from>Jani</from>
                <heading>Reminder</heading>
                <body>Don/'t forget me this weekend!</body>
                </note>'''
    data = ET.Element(xml_data)
    mydata = ET.tostring(data)
    # for i in mydata.__getattribute__:
    #     print i.find('note').get('name')

    myfile = open("items2.xml", "w")
    myfile.write(mydata)
    return HttpResponse(mydata)


"""=============================================================="""
""">>>>>> Details of line item destination >>>>>>"""
""">>>>>> Details of line item destination not in use >>>>>>"""
"""=============================================================="""
def EditlineitemDetination(request, id=None):
    line_edit = get_object_or_404(ManufactureLineTable, id=id)

    shipment_position = get_object_or_404(ManufactureLineTable, id=id)
    print (line_edit.id)
    update_create_orderposition = CreateManufacturerOrder.objects.get(
        invoice_no=line_edit.manufacturer_table)
    print (update_create_orderposition)
    form = WorkProgressBarForm(request.POST or None)
    if request.method == 'POST':
        instance2 = form.save(commit=False)
        # if instance2.
        instance2.lineitem_id = line_edit.id
        instance2.step_position = instance.step_position
        update_create_orderposition.step_position = instance.step_position
        update_create_orderposition.save()
        instance2.save()
        messages.success(request, "SuccessfullyUpdate Update ")
        return HttpResponseRedirect(reverse('mf_list'))
    return render(request, "mfStore/editwp.html", {"form": form, "order_position": int(line_edit.step_position)})


"""=============================================================="""
""">>>>>> create  invoice  >>>>>>"""
"""=============================================================="""

def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    print (template)
    html = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
    # pdf = pisa.CreatePDF(html, result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None
def barcodegenerator():
    num= random.randrange(1,1000000000000)
    image = barcode.get_barcode_class('ean13')
    # print image
    # imagge_bar = image(u'{}'.format(num))
    return num


def generate_view(request, id):
    template = get_template('mfStore/invoice.html')  # url pdf
    order_edit = get_object_or_404(ManufactureLineTable, id=id)    
    cm_queryset =CreateManufacturerOrder.objects.filter(linetable=order_edit)    
    order_item=CreateManufacturerOrder.objects.filter(
        linetable=order_edit)[0]
    linetable_queryset = ManufactureLineTable.objects.filter(
        manufacturer_table=order_item)
        
    address_query = Address.objects.filter(manufacturer_name=order_item)
    context = {
        "invoice_id": cm_queryset,
        "customer_name": "John Cooper",
        "address_query": address_query,
        "amount": 1399.99,
        "today": "Today",
        'linetable_queryset': linetable_queryset,
        "barcode": barcodegenerator()
    }    
    pdf = render_to_pdf('mfStore/invoice.html', context)
    if pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        filename = 'packgingslip_%s.pdf' % (order_item)
        content = "inline; filename=%s" % (filename)
        download = request.GET.get("download")
        if download:
            content = "attachment; filename='%s'" % (filename)
        response['Content-Disposition'] = content
        return response
    return HttpResponse("Not found")
    

# def generate_view(request, *args, **kwargs):    
#     pass
#     template = get_template('invoice.html')
#     context = {
#             "invoice_id": 123,
#             "customer_name": "John Cooper",
#             "amount": 1399.99,
#             "today": "Today",
#         }
#     html = template.render(context, request)
#     config = pdfkit.configuration(
#         wkhtmltopdf="C:/Program Files/wkhtmltopdf/bin/wkhtmltopdf.exe")
#     pdfkit.from_string(html, 'out.pdf',
#                        configuration=config,)
#     pdf = open("out.pdf")
#     response = HttpResponse(pdf.read(), content_type='application/pdf')
#     response['Content-Disposition'] = 'attachment; filename=output.pdf'
#     pdf.close()
#     os.remove("out.pdf")  # remove the locally created pdf file.
#     return response  # returns the response.
    # return HttpResponse("Not found")


# def generate_view(request, *args, **kwargs):
#     response = HttpResponse(content_type='application/pdf')
#     response['Content-Disposition'] = 'attachment; filename="somefilename.pdf"'
#     buffer = BytesIO()
#     # Create the PDF object, using the BytesIO object as its "file."
#     p = canvas.Canvas(buffer)
#     # Draw things on the PDF. Here's where the PDF generation happens.
#     # See the ReportLab documentation for the full list of functionality.
#     p.drawString(100, 100, "Hello world.")
#     # Close the PDF object cleanly.
#     p.showPage()
#     p.save()
#     # Get the value of the BytesIO buffer and write it to the response.
#     pdf = buffer.getvalue()
#     buffer.close()
#     response.write(pdf)
#     return response

#=========================by using report lab

# def generate_view(request, *args, **kwargs):
#     template = get_template(
#         "invoice.html")
#     response = template.render({}, template)
#     return response

def GeneratepackingSlip(request, *args, **kwargs):
    template = get_template("packing_slip.html") #url topdf
    
    # context = Context({'pagesize': 'A4'})
    context ={'pagesize': 'A4'}
    import cStringIO as StringIO
    html = template.render(context)
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html), dest=result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    else:
        return HttpResponse('Errors')
        # return None


# def generate_view(request, *args, **kwargs):
#     template = get_template('invoice.html')
#     context = {
#         "invoice_id": "12345",
#         "name": "shivam",
#         "amount": 139.99,
#         "date": "today"
#     }
#     html = template.render(context)
#     print ("html", html)
#     pdf = render_to_pdf('invoice.html', context)
#     if pdf:
#         response = HttpResponse(pdf, content_type='application/pdf')
#     return response
def activate(request, uidb64, token, backend='django.contrib.auth.backends.ModelBackend'):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
        # return redirect('home')
        return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')
