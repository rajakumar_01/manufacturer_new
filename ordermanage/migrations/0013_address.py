# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-10-03 06:19
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ordermanage', '0012_auto_20180927_1339'),
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address', models.CharField(max_length=256, null=True)),
                ('city', models.CharField(max_length=50, null=True)),
                ('state', models.CharField(max_length=3, null=True)),
                ('zip', models.IntegerField(null=True)),
                ('phone_number', models.CharField(max_length=20, null=True)),
                ('po_number', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ordermanage.Order')),
            ],
        ),
    ]
