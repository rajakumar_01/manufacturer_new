from django.urls import path, include
from django.contrib import admin
import django.core.handlers.wsgi
from . import views 
from django.conf.urls.static import static
from django.conf import settings
from .forms import LoginForm, SignUpForm, AuthenticationForm, EmailAuthenticationForm
from django.contrib.auth import views as auth_views
from . import api
# from rest_framework_jwt.views import obtain_jwt_token
from rest_framework.authtoken.views import obtain_auth_token
urlpatterns = [
    
 
    path('admin/',admin.site.urls),
    # path('<str:hmac>/<str:locale>/<str:session>/<str:shop>/<str:timestamp>/',include('shop.urls'))
    path('',views.user_login, name='mvp'),
    path('ordermanage/',include('ordermanage.urls')),
    path('signup/',views.signup,name='signup'),
    path('logout/',django.contrib.auth.views.LogoutView.as_view(),name='logout'),
    path('shop/',include('shop.urls')),
    path('shopify_app/',include('shopify_admin.urls')),
    
    path('mf_store/',include('mfStore.urls')),



    #PASSWORD VIEWS - AUTH BASED
    path('password_reset/',auth_views.PasswordResetView.as_view(),name='password_reset'),
    path('password_reset/done/',auth_views.PasswordResetDoneView.as_view(),name='password_reset_done'),
    path('reset/<uidb64>/<token>/',auth_views.PasswordResetConfirmView.as_view(),name='password_reset_confirm'),
    path('reset/done/',auth_views.PasswordResetCompleteView.as_view(),name='password_reset_complete'),
    path('auth/',include('rest_framework_social_oauth2.urls')),#heroku comment

#####################################react app###############################################    
    path('frontend/',include('frontend.urls')),
#############################################################################################
#############################################################intregting api with #############################    
    path('apitoken/', obtain_auth_token, name='api_token_auth'),
    path('api/',api.auth.as_view()),
    path('api/signup/',api.Signup.as_view()),
    path('api/product_list/',api.Product_list.as_view()),
    path('api/product_list/<str:rate>/',api.get_by_count.as_view()),
    path('api/shopify/',api.getshopify.as_view())
#########################################################################################
    
]


"""for Lodaing Static files in django """

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

"""End Of  Lodaing Static files in django """