import React,{Component} from 'react'
import { Form,Col,Button } from 'react-bootstrap'
import { Redirect } from 'react-router-dom'

class Shopify extends Component{
    constructor(){
        super()
        this.state={
            store:""
        }
        this.onupdate = this.onupdate.bind(this)
        this.onsubmit = this.onsubmit.bind(this)
    }
    componentDidMount(){
        const token =  "Token "+localStorage.getItem('token')
        console.log(localStorage.getItem('token'))
        const requestOptions = {
            method: 'GET',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': token,
                'Allow-Control-Allow-Origin': '*'
            },
            // body: JSON.stringify({data:this.props.location.state.token})    
            
        }
        
        fetch(process.env.URL+'api/shopify/', requestOptions)
            .then(response => response.json())
            .then(data => this.setState(data))
            .then()
    }
    
    onupdate(event){
        this.setState({[event.target.name]:event.target.value})
    }
    onsubmit(){
        const token =  "Token "+localStorage.getItem('token')
            const requestOptions = {
                method: 'post',
                headers: { 
                    'Content-Type': 'application/json',
                    'Authorization': token,
                },
                body: JSON.stringify(this.state)
            }
            fetch(process.env.URL+'api/shopify/', requestOptions)
                .then(response => response.json())
                .then(<Redirect to={{pathname:"/home",state:{token:localStorage.getItem('token')}}}/>)
            
        
    }
    render(){
        console.log(this.state)
        return(
            this.state.access_token !== undefined ?<Redirect to={{pathname:"/home",state:{token:localStorage.getItem('token')}}}/>:
            <div>
                 <Form>
                        <Form.Row>
                            <Col>
                                <Form.Label>Shopify store</Form.Label>
                                <Form.Control placeholder="Username" name="store" onChange={this.onupdate}/>
                            </Col>
                            <Button type='button' onClick={this.onsubmit}>Signup</Button>  
                        </Form.Row>
                </Form>
            </div>
        )
    }
}
export default Shopify