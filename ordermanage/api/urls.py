from django.conf.urls import url, include
from django.contrib import admin
from views import OrderListAPIView

urlpatterns = [
    url(r'^$', OrderListAPIView.as_view(), name='index' )
]

