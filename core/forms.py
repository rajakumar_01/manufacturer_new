from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.forms import ValidationError


# class LoginForm(AuthenticationForm):
#     username = forms.CharField(required=True)
#     password = forms.CharField(required=True, widget=forms.PasswordInput)
#     def clean_username(self):
#         username = self.data['username']
#         if '@' in username:
#             try:
#                 username = User.objects.get(email=username).username
#             except ObjectDoesNotExist:
#                 raise ValidationError(
#                     self.error_messages['invalid_login'],
#                     code='invalid_login',
#                     params={'username':self.username_field.verbose_name},
#                 ) 
#                 print ("ObjectDoesNotExist")
#                 return None
#         return username
class LoginForm(forms.Form):
    username = forms.CharField(required ="True")
    password = forms.CharField(required ="True", widget=forms.PasswordInput)

Select_Group= [
    
    ('warehouse', 'warehouse'),   
    ('manufacturer', 'manufacturer'), 
    ]
    
class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    # Select_Group = forms.CharField(label='Select Group', widget=forms.RadioSelect(choices=Select_Group))
    Select_Group = forms.ChoiceField(required=True, widget=forms.RadioSelect( attrs={'class': 'Radio'}), choices= Select_Group)
    print(Select_Group)
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'Select_Group', 'password1', 'password2',)

class EmailAuthenticationForm(AuthenticationForm):
    def clean_username(self):
        username = self.data['username']
        if '@' in username:
            try:
                username = User.objects.get(email=username).username
            except ObjectDoesNotExist as e:
                raise ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username':self.username_field.verbose_name},
                )
        return username