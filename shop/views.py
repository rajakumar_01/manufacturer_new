from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse
from django.urls import reverse
from .models import Category, Manufacturer, Company, CompanyName,Variant,Products
# from ordermanage.models import ShopifyAccessToken
from django.views import generic
from .forms import SimpleForm, CreateCategory, AddCompanyForm, AddCompanySkuForm
from django.urls import reverse
from django.contrib import messages
from django.contrib.auth.models import User, Permission, Group
from django.contrib.auth.decorators import login_required
global group1
import json, urllib3, shopify, requests
from shopify_admin.views import Edit
# group1 =Group.objects.get(user = request.user)

# from ebayshop.views import ebayaddProduct
# from amazon.views import addProductAmazon
from core import settings
from shopify_admin.views import addproduct

@login_required(login_url="/")  
def product_list(request, category_slug=None):
    print(request.user.is_authenticated)
    
    print(category_slug)
    category = None
    # print("useremail:",request.user.email)
    categories = Category.objects.all()
    total = Variant.objects.all().count()
    """Group Based Templates"""
    if request.user in Group.objects.get(name="manufacturer").user_set.all():
        print('a')
        group1 = "manufacturer"
    elif request.user.is_superuser:
        print('b')
        group1 = "superuser"
    else:
        print('c')
        group1 = "warehouse"

    """End Group Based Templates"""


    if(total==0):
        return render(request, 'shop/product/list.html',{'group1':group1})
    red = Variant.objects.filter(inventory_item_quantity__lt = 20).count()
    print (red)
    yellow = Variant.objects.filter(inventory_item_quantity__range =(20, 99) ).count()
    print (yellow) 
    remain = total-(red+yellow)  
    red_percent = red*100/total
    print (red_percent)
    yellow_percent = yellow*100/total
    print (yellow_percent)
    rest = 100 -(red_percent +yellow_percent)
    products = Variant.objects.all()
    group = Group.objects.get(name="warehouse").user_set.all()
    
    
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)
    print('d')
    return render(request, 'shop/product/list.html', {'remain':remain, 'yellow': yellow, 'red': red ,'category': category,'categories': categories,'products': products, 'yellow_percent':yellow_percent, 'red_percent':red_percent, 'rest':rest, 'group':group, 'group1':group1,})

@login_required(login_url="/")
def product_detail(request, id, slug):
    """Group Based Templates"""
    print("inside delta'")
    if request.user in Group.objects.get(name="indianteam").user_set.all():
        group1 = "indianteam"
    elif request.user.is_superuser:
        group1 = "superuser"
    else:
        group1 = "warehouse"

    """End Group Based Templates"""
    print(id)
    print(slug)
    variant = get_object_or_404(Variant, variant_id=id, slug=slug)
    return render(request,
                  'shop/product/detail.html',
                  {'product': variant,'group1':group1})

@login_required(login_url="/")
def company_stock(request, category_slug=None):

    # group1 =Group.objects.get(name = "warehouse")
    """Group Based Templates"""
    if request.user in Group.objects.get(name="indianteam").user_set.all():
        group1 = "indianteam"
    elif request.user.is_superuser:
        group1 = "superuser"
    else:
        group1 = "warehouse"

    """End Group Based Templates"""

    # group = Group.objects.get(name="warehouse").user_set.all()
    # if not request.user.is_superuser and not (request.user in group):
    #     raise Http404
    # if not request.user.is_authenticated:
    #     raise Http404

    companys = Company.objects.all()
    total = Variant.objects.all().count()
    red = Variant.objects.filter(inventory_item_quantity__lt = 20).count()
    yellow = Variant.objects.filter(inventory_item_quantity__range =(20, 99) ).count()
    
    red_percent = red*100/total
    print (red_percent)
    yellow_percent = yellow*100/total
    print (yellow_percent)
    rest = 100 -(red_percent +yellow_percent)    
    return render(request, 'shop/product/company_stock.html', {'companys': companys,
                                                                'yellow_percent':yellow_percent,
                                                                'red_percent':red_percent,
                                                                'rest':rest, "group1":group1,'red':red,'yellow':yellow,}) 

@login_required(login_url="/")
def edit(request, id=None):

    # group1 =Group.objects.get(name = "warehouse")
    """Group Based Templates"""
    if request.user in Group.objects.get(name="indianteam").user_set.all():
        group1 = "indianteam"
    elif request.user.is_superuser:
        group1 = "superuser"
    else:
        group1 = "warehouse"

    """End Group Based Templates"""
    group = Group.objects.get(name="warehouse").user_set.all()
    if not request.user.is_superuser and not (request.user in group):
        return HttpResponseRedirect(reverse('404'))
    instance = get_object_or_404(Variant, variant_id=id)
    print(instance)
    form = SimpleForm(request.POST or None, request.FILES or None, instance=instance)
    print(form.data)
    
    if form.is_valid():
        print("valid fprm")
        instance=form.save(commit=False)
        instance.inventory_item_quantity = request.POST.get('inventory_item_quantity')
        instance.save()
        company_sku = Company.objects.filter(Product_SKU =instance.variant_id)
        print ('==========================')
        print (company_sku)
        #####################
        Edit(instance,quantity=form.data['inventory_item_quantity'],name=form.data['title'],variant_sku=form.data['variant_sku'])
        for k in company_sku:
            print (k.company_name)
            print ('-------------------------')
            if k.company_name == "Shopify":
                print (k.company_sku)
                print (k.Product_SKU.quantity)
                query_set = ShopifyProductDetails.objects.get(product_sku = k.company_sku)
                print (query_set.product_sku)
                product_sku= query_set.product_sku            
                po = str(query_set.variants_id)
                qty= k.Product_SKU.quantity
                price = str(k.Product_SKU.price)
                print (price)
                print (po)          
                API_KEY = 'a5aab5ac3960fc8a59f975825ab6b759'
                PASSWORD = 'adc4aed5302ff9b07e10d5f864806d4a'                
                headers  = {'Content-type':'application/json'}
                datas = {
                        "variant": {
                            "id": sttr(query_set.variants_id),
                            "title": "This is new title",
                            "price": price,
                            "sku": product_sku,
                            "position": 1,
                            "inventory_policy": "deny",
                            "compare_at_price": "389.00",
                            "fulfillment_service": "manual",
                            "inventory_management": "shopify",
                            "option1": "option Title",
                            "taxable": True,
                            "barcode": "",
                            "grams": 20000,                                
                            "inventory_quantity": qty,
                            "weight": 20,
                            "weight_unit": "kg",                                                                
                            "requires_shipping": True,                            
                        }
                    }  
            response = requests.put('https://ordermanage.myshopify.com/admin/variants/'+po+'.json', headers =headers, auth=(API_KEY, PASSWORD ), verify=True, data= json.dumps(datas))
            
            print (response)
        
        messages.success(request, "Successfully Edit")
        #return HttpResponseRedirect(instance.get_absolute_url())
        return HttpResponseRedirect(reverse('shop:product_list'))
    context = {
        "instance" :instance,
        "form":form,
        "group1":group1,
        }

    return render(request, "shop/product/edit.html" , context)


@login_required(login_url="/")
def create(request): 
    Category_obj = Category.objects.all()
    form = CreateForm(request.POST or None , request.FILES or None)
    group = Group.objects.get(name="warehouse").user_set.all()
    # group1 =Group.objects.get(name="warehouse")
    """Group Based Templates"""
    if request.user in Group.objects.get(name="indianteam").user_set.all():
        group1 = "indianteam"
    elif request.user.is_superuser:
        group1 = "superuser"
    else:
        group1 = "warehouse"

    """End Group Based Templates"""
    if not request.user.is_superuser and not (request.user in group):
        # return HttpResponseRedirect(reverse('404'))
        messages.success(request, 'Not Allow')
        return HttpResponseRedirect(reverse('shop:product_list'))
        
    if not request.user.is_authenticated:
        raise Http404
    Product_SKU=request.POST.get('Product_SKU') 
    print (Product_SKU)
    #Shopify Api Integrations
    API_KEY = 'a5aab5ac3960fc8a59f975825ab6b759'
    PASSWORD = 'adc4aed5302ff9b07e10d5f864806d4a'
    headers  = {'Content-type':'application/json'}
    if request.method == "POST":
        # form = CreateForm(request.POST, request.FILES)
        if form.is_valid():
            idd=addproduct(request,form)
            instance = form.save(commit=False)  
            instance.slug = request.POST.get('name')
            instance.quantity = request.POST.get('available_quentity')  
            instance.Product_SKU = idd
            instance.save()            
            messages.success(request, "successfully created")
            #return HttpResponseRedirect(reverse('shop:product_list'))
            return HttpResponseRedirect(reverse('shop:create'))
            
    context = {
        "form":form,
        "Category_obj":Category_obj,
        "group1":group1
        }
    
    return render(request, "shop/product/create.html" , context)


@login_required(login_url="/")
def category(request):

    # group1 =Group.objects.get(name="warehouse")
    """Group Based Templates"""
    if request.user in Group.objects.get(name="indianteam").user_set.all():
        group1 = "indianteam"
    elif request.user.is_superuser:
        group1 = "superuser"
    else:
        group1 = "warehouse"

    """End Group Based Templates"""
    all_category = Category.objects.all()
    form = CreateCategory(request.POST or None)
    group = Group.objects.get(name="warehouse").user_set.all()
    if not request.user.is_superuser and not (request.user in group):
        return HttpResponseRedirect(reverse('404'))
    else:
        if request.method == "POST":
            form = CreateCategory(request.POST)
            if form.is_valid():
                instance = form.save(commit=False)
                instance.slug = request.POST.get('name')
                instance.save()
                messages.success(request, "successfully created")
            # return HttpResponseRedirect(reverse('index'))
                return HttpResponseRedirect(reverse('shop:category'))
    context = {'form':form,

                'all_category':all_category,
                'group1':group1
    }
    return render(request, "shop/product/category.html" , context)


@login_required(login_url="/")
def inventory_hundred_Percent(request):
    # group1 =Group.objects.get(name="warehouse")
    """Group Based Templates"""
    if request.user in Group.objects.get(name="indianteam").user_set.all():
        group1 = "indianteam"
    elif request.user.is_superuser:
        group1 = "superuser"
    else:
        group1 = "warehouse"

    """End Group Based Templates"""
    inventory_hundred = Variant.objects.filter(inventory_item_quantity__gte = 100)
    return render(request, "shop/product/inventory_hundred.html" , {'inventory_hundred':inventory_hundred, 'group1':group1})


@login_required(login_url="/")
def inventory_twenty(request):
    # group1 =Group.objects.get(name = "warehouse")
    """Group Based Templates"""
    if request.user in Group.objects.get(name="indianteam").user_set.all():
        group1 = "indianteam"
    elif request.user.is_superuser:
        group1 = "superuser"
    else:
        group1 = "warehouse"
 
    """End Group Based Templates"""  


    inventory_twenty = Variant.objects.filter(inventory_item_quantity__lt = 20)
    return render(request, "shop/product/inventory_twenty.html" , {'inventory_twenty':inventory_twenty,'group1':group1})


@login_required(login_url="/")
def inventory_inrange(request): 
    """Group Based Templates"""
    if request.user in Group.objects.get(name="indianteam").user_set.all():
        group1 = "indianteam"
    elif request.user.is_superuser:
        group1 = "superuser"
    else:
        group1 = "warehouse"

    """End Group Based Templates"""
    inventory_inrange = Variant.objects.filter(inventory_item_quantity__range =(20, 99))
    return render(request, "shop/product/inventory_inrange.html" , {'inventory_inrange':inventory_inrange,'group1':group1})

@login_required(login_url="/")
def AddCompanySku(request):

    # group1 =Group.objects.get(name = "warehouse")
    """Group Based Templates"""
    if request.user in Group.objects.get(name="indianteam").user_set.all():
        group1 = "indianteam"
    elif request.user.is_superuser:
        group1 = "superuser"
    else:
        group1 = "warehouse"

    """End Group Based Templates"""

    form = AddCompanySkuForm(request.POST or None)

    product_Sku= Variant.objects.all()
    if request.method =="POST":
        print ("Post is working")
        if form.is_valid():
            instance=form.save(commit=False)
            print (instance.company_name)
            instance.save()
            if str(instance.company_name) == "Shopify":
                print ('product for shopify')
                API_KEY = 'a5aab5ac3960fc8a59f975825ab6b759'
                PASSWORD = 'adc4aed5302ff9b07e10d5f864806d4a'
                headers  = {'Content-type':'application/json'}
                shopify_instance = Company.objects.get(company_sku = instance.company_sku)
                print (instance.Product_SKU.name)
                datas = {
                    "product": {
                                "title": instance.Product_SKU.name,
                                "body_html": instance.Product_SKU.description,
                                "vendor": "ordermanage",# manu_id
                                "product_type": instance.Product_SKU.slug,
                                "images": [
                                                {
                                                    "attachment": "R0lGODlhbgCMAPf/APbr48VySrxTO7IgKt2qmKQdJeK8lsFjROG5p/nz7Zg3\nMNmnd7Q1MLNVS9GId71hSJMZIuzTu4UtKbeEeakhKMl8U8WYjfr18YQaIbAf\nKKwhKdKzqpQtLebFortOOejKrOjZ1Mt7aMNpVbAqLLV7bsNqR+3WwMqEWenN\nsZYxL/Ddy/Pm2e7ZxLlUQrIjNPXp3bU5MbhENbEtLtqhj5ZQTfHh0bMxL7Ip\nNsNyUYkZIrZJPcqGdYIUHb5aPKkeJnoUHd2yiJkiLKYiKLRFOsyJXKVDO8up\nosFaS+TBnK4kKti5sNaYg/z49aqYl5kqLrljUtORfMOlo/36+H4ZH8yDYq0f\nKKFYTaU9MrY8MrZBNXwXHpgaIdGVYu/byLZNP9SaZLIyOuXCtHkpJst+Wpcm\nLMyCa8BfP9GMb9KQdPDd1PPk1sd5VP79/L5dQZ0bI9+ymqssK9WcfIoXHdzG\nxdWWfteib79lSr1YP86MYurQxKdcUKdMQr5ZSfPs6YEZH8uhl4oWIenMuurQ\nttmejaqoqsqBVaAcJLlJN5kvMLlZRMNsSL5fRak0LbdQQMVvSPjw6cJnRpkf\nKtmjhvfu5cJtT7IuOMVvWLY/M/37+o0YH9ibhtSYdObErc6HarM9NnYSGNGR\navLi09unje3WyeO8rsVrT7tdRtK3uffu6NWeaL9pTJIjJrM4NPbx8cdyX7M7\nPYYVHu7j4KgoNJAYIKtkV5o9MsOcldicis+RYNutfrhFOZ0hJbqinZ8bI8h5\nUObFuOfItJsfJrJfUOfIqc+PXqQtK8RnSbA4Mcd3Tm0SGbpXQ8aqp7RLNs+s\novHfzpVhV9iggMd1TLtbRKUdKXEQFsd4XrZRPLIgMZUeJ+jKvrAlK6AhJ65A\nMpMpKuC3j5obIsRwS7hAN8l/YtvDvnYXHbAoLI47SIUsOMenorF4gO/m4+fH\npo4vLZ8oKMukqp0cJbhVSMV2UuPR0bAfMLIrLrg/OcJwT8h+Vt+wn8eurLlh\nQrIfKHQOHHQOHf///////yH5BAEAAP8ALAAAAABuAIwAAAj/AP8JHDhQXjpz\n/PopXNiPn0OHDRMmbKhQIsOJFS1SxAhxI8SHFzVeDBnx48iNBAeeOkcxokeX\nFRdOnAlSokaaLXNujJkxo8iYHRkKtWkzZSsaOXkAWsoUECynsHgoqEW1qtVa\nU7Mq2Mq1K9cUW8GKTUG2rNkUHNByWMuWLdWva7t1W7UKG4S7eO/ycEhQHgaK\nsL4VGGyocGE3br5929KuxQFFkEtIlgypsuUDmDMfWGRmUZvPoEHfGU36jgDT\nLQSoVt3IQ2sPsL0IUNZGlZ0H0lo00jEkCytWMspdGzBgn/F9EBIWnKIQlqHB\nhA0bQpx48Z7UAkoEcMTdUeTJJSxf/4akOTNnzqHb3GkjrUdp0gKwq77jWdod\nO7dNKWvhRUcWT6zYQI82xB03AAQNCdTKX/xAAB10hfVCnRtbVIhIAy14oJoZ\nAXS4XXfdQaYIeOGJRx555Z1nRnrqqUeaMtIYY8dmn7Vg2yK57TYEgAzIQGBx\nxyXHj0A0OOTggxFKSN1iWwTTAIYanpYdMtFE4+GVIHrn3XeUmVhZeWiIMoOY\nnVQDGiTgKALJjIssIsADt0mjjI6+AXcDgQYi2M8/7ijEwzRIFmBIL9NVV+EW\nVzyZ4Wqj9RBABchQWeWkV3aY5ZYjjgieeKL446mnjxwAiZVpliAjZqblt19/\n/7HCwIAFGv+X3J4s9fMckoYhphiTQTwJ5Wqn9dDDAWuMUUEFviTrS6STVlmp\npVmKqCkOn34aB6TIBAAOJeHZAYl6ptixSCL8edGbq8HFeqBDcygEyIOCGqYk\nkxUW4euiq7knbA/gUDHGv//ec2wFayQbaQWinOCslVhmSUq1/gCDLJXacgtJ\nCYu4J66cjbAKoA3CxapnOgm9g+ughdK7xYX3Rinlvj2YYcYanVBBhTg2Axzw\nG4/4k4bBzDZbKRUQP1LIsRSX6sgBZtwhzQP68ccbj7AWty4/5igEoaC9dK3r\noVtgs4evvzKqb8wyQ0JFJzXXbDMVcQBQLTDGVmCssstKGs09oPT/jQcRoBw9\nMamKgEOeeg/gqBtvdVZSDnHFIQgRD4RxXWhiYEOQKNn4zncHzDIzHc0ZpHdy\nRicIQOypKDf7q3Pd96ABzSab+E1EIYIvS2o0ijA92gPZiCB1qwL+iJxL78Z7\n2NeHQrAK2YrCZva+bcgcujFUQIEG6WigonoCdLT9tr9UbIIAMMCEkkYacvvT\nxSgsBPKGJKBEAw4yjhx+hyn+PAJFfztyVdWOt5B3RehyimneFuwFvQxFyTSf\n25f1zCAqSFACDXTQ3gwSoDoElI5tZyBAINqnuhJ+Kg9vOIOaVnSHT5ECHucK\n0OMiBxJAPCdXmGseBLoBvei5rFEStB5m/yBhjFJUIw50oIMoLvCpFRAADduj\nwxvUYMIqmvARCBiDeiwRBk+lQQTEq5qQ3CWdJSkGAlu4y9h66EBgAbF6QhSV\nMUpQilKcQRNLwIenfpFEJebBioC0ohrQQJ8QhMIfSwhgj2YouYTYUEmGqhBe\nFNBDH5otgmgLnRyLWMdq0GEGCMCHJjSBjzQE8pSChMLTCJBI4pXDBeuiiA1T\nprK7PK+SUPphsIQ1wSEag5OUKIUlyiAmAowClci0YizKILUAFi+WDQEEJOmF\nxlnMYnOVbOP0gkjBTdZRmDiwhCuywcRkmtOEpHjC1DzBABto4xqN5AcgdEXN\nNO4Ql0+CB2xctv9LM2SSgpXhZB0t0QlT+iMUkzinQquFihD452P0gGdGAPGN\nHKYxjbOAwBpxqU9+ApGXQgyoQDWRgASwoAMGMMAHDrnQhc5AkQPSU0NgYVF7\nQmAWKcBnPvc5HwGcbUVxJCInEfACQXQACUhFQkqRwAIOttScv9ABO21wA8k1\np5Z3mYXYdNqAjvLzbHDUpFCNIQoUdGAdHUhrUg2gVAOg4AXmvEAaOPEGaCCA\nAASQxBtIYYIq5kEHAaKHVfsRGB3eNBPYxKdXGVWGUnAzdOSxgyg+MIxhoDWt\nal3rUlXABEBeYBQIiMMm0AAKPBBAE1A4nTjWEIAzvGEFqsvDEHqEjZj/wMKw\n1rwlVxerGkv4AxVoAOkEmXGMOKDgA8i1LFrRioSjKrWtKRVEQlXHBBSKQhLQ\nEG3tCHCLJaSWClD0zgHO8LBqDeIYNsDGTG4ryZtak4G7lZ6G2sBSfyCAaTK7\nAzfgQIEzoOC/yKVsZS+bWeim1BsdqEG10oCANxDgDZwIRHa3O4hbaA91nlKB\nKA7QBhHo0VPwCFBtAdNea86CZVztKk8FUN5PjQIHxKWABihQBkHY+L/HTa5l\nMetcAxvAG94wQAQAkA1SIIAUBvUHdkVLgBkMwrvkPSEkVtSCJ/yCAJ5gZ20l\nwgObziITGk3xTqUHhWoxYQVdAIYINMBmO0TA/8aCwHGOBbwOAvc4pXj2RieY\nIY69ttgfpJBEHOLQ5ArTAQ2SaPAb4lAC33XsoaxYhUx4kFVrZoKSYlYxbOzg\nPX8kAM1d6AILOuEDDQzBBCaIwJvhjOMAU7bOmE0qdMUhhFozQhVxiMWnuiAJ\nQTfZyahFQydWGwA1cbiZAJL0Qiht6UzoVsxetUQaJhEKZzhDBdh+A5s9AQxU\nq3rVN241ne0sa1rXWgjbqLUd3uqPUYhCFNDAxwzm3d3vjgF/vTvAHegUaYbw\nwMSZyAR8oX0I2BwiC2eoQQ2srYJA6IDNb2ABqr39bVYDWMfkRgIVzs1xdEOD\nCjhQ4nXlPe9BaOLQNf+rRjQc0eg2DM8TyvZTs3mY6Xwy4xI2YLMGdIAAhTvD\nFWzuhKhZIHGKq9riF381rDtQho53/Bjpboc1OiEJktMbtaplrbHboCOYT9rS\nOdhopocwgiRowOw6L0MNCKCBKjwA26IW9cRTXfE4i1vAlpUEHJze8XTXehvc\n2AQ05k3vDHaiDGNYeaPNoAzGxbwf/86EHDCd4kbsyBMySII2NH92nevg4TbI\nA7ZVEGqiF93ocLb7nIdhgGMIoROW4Dvft2GHOqQiDoM3+YWJnT8O7yYL3fgI\nDwK+CrFX0lwBctUxtLH55qNd5xkYxMKvDffSn/7b4L47JYQgjnW0XvZOv0L/\nKmz/BS5sIg5QvtkavDPlO/Am+FzOBCBqgU8veEJA9LCBDRjQznIw3/lJEIBs\n5gqhUIALN3rWR3QTh31IFwcUkAiV1QEOCH4ddw8LkAqpUH5cgAtnIGzikHgs\nxzSW1w3+Jgc0Bz32Rw8DoA3lQA8yIAP6xwoj4H//B4BJYAOjoAZqYIDWRn0J\nuIB1Z3fHQAGdgHeJQIEcxwwLQH5csIHEQARE4C9aRx49oAPw5ydyIHaANUPE\nwXwtmH/6Vw5iKIb/F4DaoAGisAIroIM7WG0MR3pDd3qoJwjVQAEUAAdvEGAG\nsHcUgITFgAtLmIFNiAtQeAInMAa+UGwiyAEW8QMc//AkgKUNx7EPkLOCLOiC\nNiADIzCDY0iDm2cHLxCKbNiGPueDcVh02McJ/GWHjfABxyUJdigEfUiB+pAL\ndVAHX1B+uPCERHAChSAw8QAOHMaIE6EF3MAKkjiJxlGJljgC+UcPm7iJnch8\nDJAHoRiKaqiDBRgK01d9LDB0QFiHdmiH1YACSDCE4ziLsscIdRCIGriLhfiL\naxAPOKAKtbARPFAFQKKMywg5XuiC9ACN0TiNOwAAAHCNL5CN2siN3QiHcYhq\nwCAD6WiHomAJEzmO4LcGueCOG4gLf2OIAjOPOHCPEEFT/KiMzKgNLigDABmN\nnKgL02aQB3mNCkmKB+iNCv+IBjI2Y+O4ihcZi063DcywkReYi04Yj/ewBmuA\nAyRYEbAAAVVwkv3oj9rwgizJks4okCMwCI+ACqgwCQaJkGq4hm3IjW8YakPn\nCWxmhzz5kxfJd3iwkUx4lL0ojw/QlAnxlG4glQYCOStplS8YkJuoCwnwCIY5\nCYgZljRJlqTYg9WnbTq3lm3plrGojrVWixuJgRpIDB95AgLTCCRYkjeVAXw5\nlfqXiVa5ks64QSVlmF8JljO5mAtplj4IdJE5YzpHmenYcXCwAHKJi7rIi74Y\nD7oQms1xU71QmpQ4AOVwmvoHmAH5ABcwna3pmompmAnJmDzIcGp5m2upmxMp\ni+f/Zg9AIJeCeJSG+ACHAH8OwWyzoJyUCIOnCYOAKQP4wATTeQElVZio8AiI\nCZtiSZbbuHAIUAXemZu5CZ4YyQ250KAXeJ6c2YsCYIUYwWyZUADK6QoEwAfO\nOZ8yoANSwAT4SZ37eZjXGZtjOZshoAFQ8HAHOo6TCZ5CgAfluYS4OIhPGA8C\n4AXBtxBP+WXvWZrZ4ClhYAkdmokzgAkhKqIjqp+GaaIyGaAL+XDOEAEueqC4\nGaNuKQTWAAQ1OpceCQktcAgcYFuHJQc+wJfhADFpsAPhcJpewAZKKgVL2qTV\n2ZUnKptqMApJ8ADVZqVYKpkKaodwEAflaYvAuYFE4HIe/8CIEWGhchCkJ7kE\nJQQAHGoDZcYGckqnTGqnhWmiALqYS5AEdGCAVmqgBvqiMqagquANX3qe8cCo\njpqX1iQHsAALaWogx5FkEBMO7URCmjqnTJqfJQql2LkClpAEwNCGahABapmq\nqqqgjAAE3uCgTFgC6tEIZVoRzCYHckBpJ+kBJoQA+xcCqrOpdeqpT/qf2JkF\nSQAPOdiGLoqq0QqeVOCqDUp+RMBh+7atDgELX+atPJCPKOkAJmQJ7fRH54oJ\nc7qk+amfn+qfsAkAKqB5SeAFo7CGwBCo3smWlMkMQPaqyAAJi2AaKTBpECB5\nUdFlKJk6qoMK/McHVsSwdFqnxP9aUv3JrgRghhcbCCswqp0XmdAamTtJmXHg\nqjWaCmqCIwJwsg/RrSvLA6R5HDIAAyJAAJ3mKQQAAwxwC4Akp8Iqog9bna+5\nA2V4g+kUgM/HZlUwtB2rparwYzWKB/nzAG3QtBVaq1HxA5+wl8cBA1iABTCg\nCyGgsK7Af1lrReiariTKn6ggAmTIfDfIAJuntt7pth2bjnAABHKbC74ADi13\nByfLrQG7sp/AA8dBD4EruIILAy0ABboAA66ATMHKqcMKsZ/aCNMouWrbu2vb\nthw7kdUgt3VgP41WsinwEPzwb7NgqzzwA3xrCMYBuKu7ujBwvTBAAOYEtrbr\nqQkwg5z/GLmVa7GWy7EJmo7ccGB4gAxp8i3SMLoNEXnOywOf8AmwsA/aUL3V\ni726QELJtLi3W1ICWQ7SGLm+67tCi6UeSwGb8GOFkC1L+74uAbAq+7z1Sw0F\nwACXcAmBy8H6O7sLxb22O52k4IwD2Yk0SL69a763KWOJgAQLACnFBgl267Qy\nV8H0+wnUgAEb3MMbrL/a+1SaWrNMSgpYqZUEPIY1qMICyMJtCQSB4wv2czjw\nC3mla8E6nAzcEA4+jAU/HLiJG8IAbMRW6ZLgq8S8e8BOPGM4cDtSDLqboQD4\neMV8m8VXkAV47MMeDMJP9SmLiw82oAOpicThm8IHXL6BSgEn/4AHhbAsaRLH\nMSG/e3vBjojHWRADeowFg9DHEMO9DmADDjAK1ZCaLknAhZzGaoyl3IALXHAC\nMry0cjwR8juwz0sN1OBs3HDJlpwFl8DLvMrJnqKpUADKIUoKD1DGpVzAZ3vI\nWKoIxNDKr0yysRy/dKzDP3BTChADunzJlxAOygDMJkQANlAGmMCk+CDI0KiV\nBYzGh9zEOmcDRPCEjEwlI3IACtARkmzB1JBRs9AN3KDN2mzJZQDOJRQGNmAH\nDSuiyhCYL2jGKIzKCMxmdwCFRMDIb9xo07y8V1y/14wXVxADIA3QWRDEBF0t\nBi0CAOwKgDkCmmjGpzy+anwPvbjIJ//gyBitvLNswRmVVewQ0iL9yyVt0PVA\nAIsLBfVJytK4zuXQzknADIZoiIVABNEsx8vWvN/6vJRmU6vw0T4tsyWtOvxn\nA+EABQCgpID8gqh5lQ6dxGR4yIrgi78o01MdyVY9sJ+QCd+ARlmVzT490F8N\nMTEQ1gwQDiGwPh260i2dzJ3Yu8eAO/fw2BVwD408w7UAEv9mqyubQBe1Q/98\nCCA9A38NMSLAf4JtAyFw2Gnd0Il9wmKotm0Q10o5j41svFQtc/M7CwmU1/ZU\nC559CLrwC6FdLSFA2sR9pB5anw4dvlUZDyE5j/SINKBb2RRx2ZldHUxyFxwQ\nA70d3NUCBa7/QtyljdrIvdZj6AFKGQ/oTY84YA8PnCb3ON11PQv0dN0QgA1X\noAuH4Fvc7SkIwABcC97hfdiIvdrgSwnOrd72QAkGDsHSnRDD57wS0g4NcAVb\ncN1bkAKHcAh+vd95cL3+DeABPp+pjcybeAnojQMobg8JTgmqQAlSrAjSHb8q\nOwvT0QDocOMTQAJ6UARk4M+HANr77SnY6+Egrn/tdKTjHY2LkOIqruCq8OR2\n8MYk6ScqSyiGQAI3fuNRsOVRMAEKcAjAHeT+cARD/t8g3k5HLuJHLQMMYA/r\nreAsbhv48QCUYD8NDnmSR+MF0At/YARGoOXoEAW8QAscMARhHNwh/1DmHm7m\nxZ3mxw2Y1rDicY4ft/EAlp4tlS3LkndD3ODnfp7lW14EW7AHYu4pg9C6Zc5/\njE7a+4fkad3iTy7nlW4KtC4N9hAAU47nR1IAwtAMno4Of77labQHrVDqYWC9\nis61qx7i83kIsU7plk7rppAI1G4K0UCSDp4JbgAdJNAMvv7pOL4YViAPpe4P\n+pvsy87qrT6ftQHtiUPr1K4M+9EC9nDnlOYDg+EDf+Dt3/7n6EALi0EL+VDu\nD4DsqI69ql7kjo4F7r4IpiAN8T7vjdAIdmDv74DvPsAN/O7tv14EiUECUQAC\npV4G+ovsqf7hAH6a1jDr8E7tLaAbE+8FMv//3n6S79MwBDuw7xzv6e2gGBMQ\nBadQ6gSABQ5AAA4gAodg8kOe8GduCu8O8S7/8jHfH5/HDiWRDH6QA9hwK4PB\nDfbyBLRAAtPxDbaw5X0g5mlwCXzsMwgABUdw8Aif7ocg7fEu9VP/eUPwCmDw\nAzPxA+TgBxgQ+BBgMpUjKNQR6FEwB6WuDJdw6AAQuMnO9KQNI3UP8x0DQHoP\nBmBABnuxEH4f+KAP+LitPNNRDFq+DCN/CSQt3Psb+fyXBZU/8ZevA5mv+Zqf\nAz/AED+gBeQA+r4f+DkAAShTBKAu8kFOAOFQDQV97oqu6o0g8TFP+7Vv+5Ug\nC9+q+1PQ+7//+1n/DwFF4O/osAFiDgB4DNT+UPDWC/lljgV23zF5b/vwXwny\njw3f+hE/kP1TsP36/wxNABBNeEVBp87fQYQJFS5k2NBOjGoEwvxKSOASFowZ\nscDgyHFIo0ZehrwCU9JkyUopK8nKlIkHP379+P2YMoUcBpw5deZ8RohQE6Cn\nGg4lOnRGDKRZsoS7pMPSA6YXNWLsKJLkSZOVwKhMGSTTrJf9ZNKcomXKTrQY\nevr02cSIvKJxi6aJkaVuXaZMs1ziO5UqPawnuXK9AWEW2Jhja9pMuzMd27YW\nLNga10fuZYUPkdZdqpTv575YbJQbkCHw1sEpb9wQMstwWLFkbfppjJPc/wTI\nhHhJ5r0BBGbMRzfb7ez5MwwbpTMsx5pa9eob2CBM5yETpmzGtTE8hrybN29b\nc1oBn6trc9K7nhmUy6BcOUrn0KHLcr0FQvWYMxdnb3w7t/fvwFMiFvKG0uw8\n4kRLYjkGG0RtMPlWc+GGdyCwbwtYrOsHu7K0a+K/AEO04K0CF8InBvPOg2GE\nKpZTrsHSUotwwgnnmW4LHGGBKbb9bMqhsSly082CW0QMkDLLSvQHFQFiOESX\nLGzQpkUY22swA8Lko9EFLqfBEcdvMhRrwx610OLHtJ5Rc01ahHnCzTeFkXNO\nOfWQkwQ6NNFzTz2X0GQJQAMVdJEYsBhBAyrbK/9tgBcbrCTCG7bkkstvvvwm\nzPzI7JEcNLXDCYICQhXVkAIMMdWQd0x1Y9VdiuHGA1hjhfWQQzyg9dZDYmBg\nyioSVfRKFwfYZ8ZIJ3XhGhe83OLSSwEZU78ea+pUO2wK8MFaUUMl9dReDOll\n1VXbuYIZWWOl1dZDLpGhV3YZXLTR9vZhUMJijUX2mmveYRZcQDLlsCZOp21s\nCx+uLTjbbE/11ttv3diFkSHKRReGcthtN1hgrdxH2Awk5fJefK+ZZ9lvVvXW\n2cT+ZSwHgdHCpmCYDb4WYVNL7baXbsN9FdYYbKDA4otddBdYeffZx9iPjw35\nmmlKNtnUfmXSNNqAW9b/6eWYY8YWYW0V7tYQhxWAwwege61y6OXkbdDoSUFe\nWuR3wP3akKhjUtlHlqklG+YqsjaY620VNgQDMcQQouwrX3zR6KKFZfttyKtw\n+utQnRUL2mjLYjnvtLDpu9e9/ZYZ8FK3maLwwn8OmlF3lWNc7df3gfzteaZZ\n+NTKx5y6RxJ69/333mvBwHOLQ/fhiR2SV34HS47hmnAafJ9gh3AaDMcB7LE/\nIoPY441dhOzDz94VN3DPNmoeM5drAyfK7lWH34baYetVCidBIT6C5UMhB4r2\nn3FheSANRVGCwhBmObtlbgqXyYYNyuYFAMQFCtPwQf3spxAraGBRR+Af91wX\n/zsPoCIuCCAV13yAMsWo7zIOaJHFSHEZHZABdWK4X0JoIAENLIeDCXFA2rgX\nuwG8MC6kKGGoZuaDTEhtd/vBTBoyYLYqeAEzFpihGCagEBqIQQJVGMAOEdLD\n2L0uHJdBAMIOhsTELHExwLnS/i6zAQlIQItWxKIccejGL/4wjPvw4kHSQApA\nBhKQUDCiEWE2C93dTSEW2EMjaWABhbgnA3g8SAj4cElK+kMJWoyjBK6YECtw\nUgKZ7N8ejdZHfzjgGgNY5SpnZsisJXFHikwICTLBskzUECFtxJ/FFKKETmrx\nkwixQiclYAX+mfKUCpnBEZzpzHpkS2Yxm0ViMNcjhf+QABs5uKUuD9KoTOaP\nQb80picxaExk8lCZfIxLNuBhrWnurZpjoiVCbAkBbnrTH2pbTjgZVAVyGnOY\nBylmJ9P5xXWOUS6WEB3ZqgmTazLxMk40WntQub3lbIOc7OjkQP1RUI4e9CCl\nfJ3jjCbEogDAE6KrAiKlVs+4gJF7GUDlDLLnUWCyg6Ps8GgxdyrSVK5zH/WI\noARjZjFEQhSmRCEFg9SGSqIoQadT7alOJcAOoJJUmeFA6VBIETqk+ssPKizK\nDorxwx9CdShSvapOqzpVoO7ApMocgAdcIb74HeSroEOqEn8w1mgVRR0KyEEw\nKqoctTZEquzggFsVooepskP/DwqZAAfmakpGvc4HXSXF54CWVLthALASRYhB\nFpmDd4QxsQxRQmNd61HITnWyCVHC9MTnCsY9U7dH4AM8spGQvVrsiRB4Fg/8\ncFxsJmQDHvUHLQyhWsy01rXs2MFj2ZGC6862KKRgHGY6K9zlEPdyP8AJcteo\n3ClsQCHq0AF0QdkN+HbjlxygL31hO13tMrW7lwkB0BiUoR3x4EfmrYlCNjAF\nCRAoIWmwQexQqQcyxHe+9eXAfVOQAg7k16v7jQsAHGi2Bv0gUzyQQ05Ga+Cy\n0MBEDsZgN8gQ4QnXt7oJ0QOGOZACDTeEu0aTCwC80EKhDcAHMDGHWATMsuMC\nFsVl/9GnP0Jg0kw24MUv/qUTOGDlCj8WETfGsVx2vI+UzsATIFZUaTIRk3QY\n+ZYlFq0Ce5QJHBXgdU+MRCSwEYlVBCHPQZhyn7vhhD9fWdAc2DKhKXxhRCc6\n0Yi4LOPcl6hGVUFqc4gJLGaxufKO1s2VkrOj63znOkciCKMedZ+n7ARUp1rQ\niLAyIlyNYURcONaInrWs9ci4JyJOaFYawDzP8Q+ZwAICLckbgd08i290eh9V\nCIadQw3qO5Oa1H1GNRlSjeorO2HLruZ2rLudAm+Dm9Gxcx/GXmSIMbnjH5W2\nzy2RbOzM+cENBRAWs0N9b3zXWdp8pra1r61tbXdb4N/2Nv8i5gzeIJd5Gjui\nwT+AzQ9YVGrYnNO0Agm27GBkvNnNzje+921qf/+b1QEfuMDFPe5lk/lspUG3\nWKbQCofLBBBuwNEs3C3aikcrB2TTeM81HgmOd3zf/PZ3yFPNaqSXfODF0EDK\nE9e6liZmCvJwOLD7AQhU2efSbG6zm7VgiG1ofBc+//nGgZ7vbYw67aVux4v/\nfXSSK53by/HVrzIwDZTBBANUrzpMeAAIWASeB4P/AQ9+cHjEJx7xWgDE5nLQ\neMdHXvKbg/zkMZ23H/1oFRjYPOc9v3nQ58Aw0xn9LACvO7HQAOZVf/jl0ii1\nHcXe9bPX3euftaPL5R71tIf97nsy7/o0WlP2r4/JOU7B+r5nqva7jz1EdZ97\n4qNe+bonfvCfVXvly1762beOOdLBd+Q7PCAAOw==\n",
                                                }
                                        ],
                                "variants":[
                                        {
                                            # "option1": "First",
                                            "price": int(instance.Product_SKU.price),
                                            "sku": instance.company_sku,
                                            # "inventory_item_id": 341629,
                                            "inventory_management": "shopify",
                                            "inventory_policy": "continue",
                                            "inventory_quantity": instance.Product_SKU.available_quentity,
                                        }
                                        
                                    ]
                                }
                }   
                response= requests.post("https://ordermanage.myshopify.com/admin/products.json", headers =headers, auth=(API_KEY, PASSWORD ), verify = True, data= json.dumps(datas), )
                print (response)
            elif str(instance.company_name) == "eBay":
                ebayaddProduct(request, instance)
            elif str(instance.company_name) == "Amazon":
                addProductAmazon(request)
            messages.success(request, 'Successfully Added')
            return HttpResponseRedirect(reverse('shop:addcompany'))

    else:
        form = AddCompanySkuForm()
    context={
        "form": form,
        'group1':group1,
        'product_Sku':product_Sku,
        
    }

    return render(request, "shop/product/addcompany.html" , context)

def AddCompany(request):

    # group1= Group.objects.get(name = "warehouse")
    """Group Based Templates"""
    if request.user in Group.objects.get(name="indianteam").user_set.all():
        group1 = "indianteam"
    elif request.user.is_superuser:
        group1 = "superuser"
    else:
        group1 = "warehouse"

    """End Group Based Templates"""
    form =AddCompanyForm(request.POST or None)
    
    all_company = CompanyName.objects.all()
    if request.method == 'POST':
        if form.is_valid:
            # print(form.data['companyname'])
            instance = form.save(commit=False)
            instance.save()
            messages.success(request, 'Successfully Added')
            return HttpResponseRedirect(reverse('shop:addcompany'))
        
    else:
        form = AddCompanyForm() 

    context = {'form':form,
            'all_company':all_company,
            'group1':group1
             }
    return render(request, 'shop/product/company.html', context)


#Delete Product
def product_delete(request ,id= None):
    instance = get_object_or_404(Variant, variant_id=id)
    if request.method == 'POST':
        instance.delete()
        messages.success(request, "Successfully Deleted")
    return render(request ,'shop:list',{} )


# company delete 
def company_delete(request ,id= None):
    instance = get_object_or_404(CompanyName, id=id)
    if request.method == 'POST':
        instance.delete()
        messages.success(request, "Successfully Deleted")
    return render(request ,'shop:list',{} )

def CreateProduct(request):
    API_KEY = 'a5aab5ac3960fc8a59f975825ab6b759'
    PASSWORD = 'adc4aed5302ff9b07e10d5f864806d4a'
    headers  = {'Content-type':'application/json'}

    datas = {
                "product": {
                            "title": "Burton Custom Freestyle 151",
                            "body_html": "<strong>Good snowboard!</strong>",
                            "vendor": "Burton",
                            "product_type": "Snowboard",
                             "variants":[
                                            {
                                                "option1": "First",
                                                "price": "10.00",
                                                "sku": "123"
                                            },
                                            {
                                                "option1": "Second",
                                                "price": "20.00",
                                                "sku": "123"
                                            },
                                        ],
                            "images": [
                                            {
                                                "src": "http://elementsmagazine.org/wp-content/uploads/2017/12/Info-icon.png"
                                            }
                                    ]
                            }
            }

    
   
    response= requests.post("https://ordermanage.myshopify.com/admin/products.json", headers =headers, auth=(API_KEY, PASSWORD ), verify = True, data= json.dumps(datas), )
    print (response)
    return HttpResponse(response)

################################    
#shopify LOgin View
################################

def ShopifyLogin(request):
    # response =  request.get("https://ordermanage.myshopify.com/admin/oauth/authorize?client_id='ead441fdcf80b836985b08647f6119c0'&scope=['read_products', 'read_orders']&redirect_uri='https://ordermanage.myshopify.com/admin'")
    response =  request.get("https://ordermanage.myshopify.com/admin/oauth/" , status=201)
    print (response)
    return (response)

###############################    
#shopify inventory update view
################################
def shopifyGetProduct(request):
    access_token = ShopifyAccessToken.objects.get(shopify_shop= "ordermanage.myshopify.com")  
    print (access_token.access_token)
    if request.method == 'GET':
        headers ={
            'Content-Type':'application/json',
            'X-Shopify-Access-Token':access_token.access_token,       
        }
        shop_url = "https://ordermanage.myshopify.com/admin/products.json"        
        response = requests.get(shop_url , headers = headers )
        print ("=========> shopify response")
        data = json.loads(response.content)       
        products_request = ShopifyProductDetails       
        #    
        for product in data["products"]:
            product_id = product["id"]
            print (product_id)
            product_sku = str(product["variants"][0]['sku'])
            print (product_sku)
            # for v in product_sku:
            #     k = v[1]['weight']
            #     print k

            variants_id = product["variants"][0]["id"]
            options_id  = product["options"][0]["id"]
            images_id   = '12345'

            shopify_product = products_request(product_id =product_id,
                                product_sku =product_sku,
                                variants_id =variants_id,
                                options_id =options_id,
                                images_id =images_id )
            shopify_product.save()
    return HttpResponse(response)
