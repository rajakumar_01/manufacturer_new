const Dotenv = require('dotenv-webpack');
module.exports = {
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
          {
            test: /\.css$/,
            use: [
              'style-loader',
              'css-loader'
            ]
          },
          {
            test: /\.(png|jpg|gif|jpeg|svg)$/i,
            use: [
              {
                loader: 'url-loader',
                options: {
                  limit: 12288
                }
              }
            ]
          }
        
      ],
      
    },
    plugins: [
      new Dotenv()
    ]
  };