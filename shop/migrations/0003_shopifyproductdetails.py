# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-10-16 08:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0002_auto_20180820_1756'),
    ]

    operations = [
        migrations.CreateModel(
            name='ShopifyProductDetails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_sku', models.CharField(max_length=256)),
                ('product_id', models.CharField(max_length=256)),
                ('variants_id', models.CharField(max_length=256)),
                ('options_id', models.CharField(max_length=256)),
                ('images_id', models.CharField(max_length=256)),
            ],
        ),
    ]
