# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-05-29 12:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mfStore', '0016_manufacturelinetable_step_position'),
    ]

    operations = [
        migrations.AddField(
            model_name='manufacturelinetable',
            name='mf_status',
            field=models.CharField(choices=[('Pending', 'Pending'), ('Acknowledge', 'Acknowledge'), ('Completed', 'Completed')], default='Pending', max_length=225),
        ),
    ]
