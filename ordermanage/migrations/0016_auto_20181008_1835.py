# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-10-08 13:05
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ordermanage', '0015_auto_20181008_1829'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='po',
            field=models.CharField(max_length=50, primary_key=True, serialize=False, validators=[django.core.validators.RegexValidator('^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')]),
        ),
    ]
