
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse
from .models import *

# from .models import *
from shop.models import Category,Company,CompanyName,Manufacturer,Products,Variant
import json, requests
from django.core.exceptions import ObjectDoesNotExist
def ShopifyRequest(request):
    length = len(ShopifyAccessToken.objects.all().filter(user_name=request.user.username))
    if(length==0):
        return render(request,'shopify_app/index.html')
    else:
        print("already app installed")
        return redirect('shop:product_list')

def register(request):
    
    return redirect('shopify_admin:token1')
    

def ShopifyLogin(request):
    print("hello")
    shop=request.GET.get('shopify_store')
    print(shop)
    api_key = '89d63250984d1ea3d1ea5f1fc7b920c1'
    api_secret_key = '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'
    # api_key = 'ead441fdcf80b836985b08647f6119c0'
    # api_secret_key= '8e243f0dd108f7402056831f5d72c22d'
    scope =str(['read_products'])    
    response = "https://"+shop+"/admin/oauth/authorize?client_id="+api_key +'&scope=write_products' +'&redirect_uri='+'http://127.0.0.1:8000/shopify_app/catch/'+'&state=12345'
    print(response)
    return HttpResponseRedirect(response)
# 6da11c46c98ed6c61366be2b042739a6
# 1591602491
# 4dd8c9574f672fb9c6f22fe9e0896f5d0b0214e3eb7ec2bce85e827cd99c1764
# hmac=d807d10ab6f348d2f4831d65768effda1b21b262580eb888ac554ad493f75af9&locale=en&session=97155178b071307ccd45d720b0f0b8e234c0a40f6aa815653d58a19df92d2743&shop=eknousseo.myshopify.com&timestamp=1591602574
def ShopifyRedirect(request):
    api_key = '89d63250984d1ea3d1ea5f1fc7b920c1'
    api_secret_key = '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'
    getresponse  = request.GET.get('shop')
    code= str(request.GET.get('code'))   
    print(code)
    timestamp= request.GET.get('timestamp')
    print(timestamp)
    hmac = request.GET.get('hmac')
    print(hmac)
    headers  = {'Content-type':'application/json'}
    datas = {        
        'client_id':api_key,
        'client_secret':api_secret_key,
        'code':code,
        }
    url =requests.post('https://'+getresponse+'/admin/oauth/access_token', data=datas)
    print(url.content)
    a=  json.loads(url.content)
    query_accesstoken = ShopifyAccessToken
    if a:
        instance = query_accesstoken(user_name=request.user.username,shopify_shop=getresponse ,access_token =a['access_token'], scope = a['scope'] )
        instance.save()    
    return redirect('shop:product_list')


'''=============================================================
========================retrive inventory from shopify==========
================================================================'''

def products(request):
    
    access_token = ShopifyAccessToken.objects.get(user_name=request.user)
    for a in access_token:
        access_token=a
    print(access_token)
    headers ={
        'Content-Type':'application/json',
        'X-Shopify-Access-Token':access_token.access_token,       
    }
    response=requests.get("https://"+access_token.shopify_shop+"/admin/products.json",headers= headers)
    print(response)
    data=response.json()
    # print(data)
    for key in data['products']:
        productid= key['id']
        optionid= key['options'][0]['id']
        try:
            category=Category.objects.get(name=key['product_type'])
        except ObjectDoesNotExist:
            # print(e)
            category=Category(name=key['product_type'],slug=key['product_type'])
            category.save()
        #code to save products in database
        for key in data['products']:
            values = {
                
                'title':key['title'],
                'body_html':key['body_html'],
                'vendor':key['vendor'],
                'product_type':key['product_type'],
                'handel':key['handle']
            }
            obj_stored, created = Products.objects.update_or_create(product_id=key['id'], defaults=values)
            # obj_create.save()
            for item in key['variants']:
                #create slug 
                slug    =key['title'].split(" ")
                slug="-".join(slug)
                valuess = {
                    'product_id':obj_stored if obj_stored != None else created,
                 
                    'title':item['title'],
                    'price':item['price'],
                    'variant_sku':item['sku'],
                    'compare_at_price':item['compare_at_price'],
                    'slug':slug,
                    'inventory_item_id':item['inventory_item_id'],
                    'inventory_item_quantity':item['inventory_quantity'],
                    'grams': item['grams'],
                    'weight': item['weight'],
                    'weight_unit': item['weight_unit'],
                    'inventory_policy': item['inventory_policy']
                }
                obj_variant_stored, variant_created = Variant.objects.update_or_create(variant_id=item['id'], defaults=valuess)
    return redirect('shop:product_list')

def addproduct(request,form_object):
    access_token = ShopifyAccessToken.objects.all()
    for a in access_token:
        access_token=a
    print(access_token)  
    print (access_token.access_token)
    headers ={
        'Content-Type':'application/json',
        'X-Shopify-Access-Token':access_token.access_token,       
    }
    # print(type(form_object.data['block_quantity']))
    print(form_object.data)
    datas = {
                "product": {    
                                
                                "title": form_object.data['name'],
                                "body_html": form_object.data['description'],
                                "vendor": 'ordermanaged',
                                "product_type": Category.objects.get(id=form_object.data['category']).name,
                                "tags": form_object.data['bulletpoints'],
                                "variant": {
                                    'quantity': form_object.data['quantity'],
                                    'weight': form_object.data['Product_Dimensions'],
                                    'price': form_object.data['price'],
                                    'sku': form_object.data['quantity'],
                                    # 'inventory_quantity': form_object.data['block_quantity'],
                                    'product_type': form_object.data['Material_Type'],
                                    
                                }
                            }
                }
    response= requests.post("https://"+access_token.shopify_shop+"/admin/products.json", data=json.dumps(datas), headers=headers) 
    data= response.json()  
    print(",####################################################",data)
    print(type(data))
    print()
    return data['product']['variants'][0]['id']
    # return data[id]

def Edit(instance,quantity=0,variant_sku=None,name=None):
    print('inside edit')
    access_token = ShopifyAccessToken.objects.all()
    for a in access_token:
        access_token=a
    print(access_token)
    print (access_token.access_token)
    headers ={
        'Content-Type':'application/json',
        'X-Shopify-Access-Token':access_token.access_token,       
    }
    variant_id=instance.variant_id
    datas={
        'variant': {
            'id':str(instance.variant_id),
            'title': name if name != None else instance.title,
            'sku': variant_sku if variant_sku != None else instance.variant_sku,
            'inventory_quantity':quantity
        }
    }
    print(datas)
    print("######################")
    
    response = requests.put('https://'+access_token.shopify_shop+'/admin/variants/'+str(instance.variant_id)+'.json', headers =headers, data= json.dumps(datas))
    print(response.json)