from django.db import models

# Create your models here.
class ShopifyAccessToken(models.Model):
    user_name = models.CharField(max_length=200)
    shopify_shop = models.CharField(primary_key = True, max_length =200, null=False, blank=False)
    access_token = models.CharField( max_length =200, null=False, blank=False)
    scope = models.CharField(max_length =200, null=False, blank=False)

    def __self__(self):
        return self.access_token
class ShopifyProductDetails(models.Model):
    product_sku = models.CharField(max_length=256)
    product_id= models.CharField(max_length=256)
    variants_id= models.CharField(max_length=256,unique=True)
    options_id = models.CharField(max_length=256)
    images_id = models.CharField(max_length=256)

    def __self__(self):
        return self.product_id


