import React,{Component} from 'react'
import { BrowserRouter, Route,Switch } from 'react-router-dom'
import Login from './Login'
import Signup from './signup'
import Home from './home'
import ProductTable from './ProductTableAs Card'
import Shopify from './shopify'
class App extends Component{
    constructor(){
        super()
        
    }
    render(){
        
        
        
        return(
            <BrowserRouter>
                            <Switch>
                                <Route path='/shopify' component={Shopify}/>
                                <Route path='/home' component={Home}/>
                                <Route path='/signuping' component={Signup}/>
                                <Route path='' component={Login}/>
                                
                                
                            </Switch>
            </BrowserRouter>    
        )
    }
}

export default App