# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-10-08 13:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ordermanage', '0016_auto_20181008_1835'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='po',
            field=models.CharField(max_length=50, primary_key=True, serialize=False),
        ),
    ]
