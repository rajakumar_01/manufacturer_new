from django.contrib import admin
from .models import Category, Company, Manufacturer, CompanyName,Products,Variant
# from ordermanage.models import  Order, Sku_Table, ShopifyProductDetails
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)} #prepopulated_fields:=>where the value is automatically set by using the value of other field 
admin.site.register(Category, CategoryAdmin)


# class ProductAdmin(admin.ModelAdmin):
#     list_display = ['Product_SKU','name', 'slug', 'category', 'price','available_quentity','block_quantity', 'quantity', 'available', 'created', 'updated']
#     list_filter = ['available', 'created', 'updated', 'category']
#     list_editable = ['price', 'quantity', 'available'] # that can be edited from listdisplay page
#     prepopulated_fields = {'slug': ('name',)}
# admin.site.register(Product, ProductAdmin)


#products admin configurations
class ProductsAdmin(admin.ModelAdmin):
    list_display = ['product_id','title','body_html','vendor','product_type','created_at','updated','handel']
    list_filter = ['updated']
admin.site.register(Products,ProductsAdmin)

class VariantAdmin(admin.ModelAdmin):
    list_display = ['variant_sku','product_id', 'variant_id','title','price','slug','compare_at_price','inventory_item_id','inventory_item_quantity','weight','weight_unit','grams']
    list_editable = ['price','inventory_item_quantity','compare_at_price']
    prepopulated_fields = {'slug': ('title',)}
admin.site.register(Variant,VariantAdmin)
"""company name New Admin table"""
class CompanyNameAdmin(admin.ModelAdmin):
    list_display=["companyname_id","companyname",]

admin.site.register(CompanyName, CompanyNameAdmin)

class CompanyAdmin(admin.ModelAdmin):
    list_display=["company_name","company_sku","Product_SKU" ]

admin.site.register(Company, CompanyAdmin)

# class ShopifyProductDetailsAdmin(admin.ModelAdmin):
#     list_display = ['product_sku','product_id','variants_id','options_id','images_id']
# admin.site.register(ShopifyProductDetails, ShopifyProductDetailsAdmin)

# class ManufacturerAdmin(admin.ModelAdmin):
#     list_display=["manufacturer_name","manufacturer_sku"]

# admin.site.register(Manufacturer, ManufacturerAdmin)
