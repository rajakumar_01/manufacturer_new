# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-05-02 07:25
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mfStore', '0007_auto_20190501_1854'),
    ]

    operations = [
        migrations.AlterField(
            model_name='manufacturelinetable',
            name='item_sku',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Manufacturar_linetable', to='mfStore.ManufacturerMapping'),
        ),
    ]
