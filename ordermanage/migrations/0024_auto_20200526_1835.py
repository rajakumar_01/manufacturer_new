# Generated by Django 2.2 on 2020-05-26 13:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ordermanage', '0023_auto_20181121_1606'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company1',
            name='Product_SKU',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='prduct_sku', to='shop.Variant'),
        ),
    ]
