from django.shortcuts import render, get_object_or_404, render_to_response, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse
from .forms import *
from .models import *
from shop.models import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.contrib import messages
# from rolepermissions.decorators import has_role_decorator #no need
from django.contrib.auth.models import User, Permission, Group
import json, urllib3, requests
import datetime 
from datetime import timedelta
from django.utils import timezone 
from django.core.exceptions import PermissionDenied
# from ebayshop.views import ebayfulfillOrder
# from shopify_app.views import OrderPublicFullfill
# from amazon.views import fulfillOrderAmazon


'''==========================================================================
    Project Name: OrderManage
    Name: OrderManage View
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
===========================================================================''' 

def chek_group(element):
    if element in Group.objects.get(name="indianteam").user_set.all():
        group1 = "indianteam"
    elif element.is_superuser:
        group1 = "superuser"
    elif element in Group.objects.get(name="manufacturer").user_set.all():
        group1 = 'manufacturer'
    else:
        group1 = "warehouse"
    return group1

'''==========================================
    edit views Function .
    
============================================='''


@login_required(login_url="/")
def edit(request, pk=None):   
    """For Group Based Templates"""    
    group1 = chek_group(request.user) 
    """End Group Based Templates"""
    group = Group.objects.get(name="warehouse").user_set.all()    
    if request.user in group and request.user.is_active :
        return active_edit(request)  
    else:
        queryset = Order.objects.filter(po= pk)
        instance = get_object_or_404(Order, pk=pk)
        form = SimpleForm(request.POST or None, instance=instance)
        PickUpDate = request.POST.get("pickup_date")        
        status = request.POST.get("STATUS")
        asn_sent= request.POST.get("asn_sent")
        remark_tbl= remark
        pickup = Order.objects.filter(pickup_date= instance.pickup_date)
        for d in pickup:
            PickUpDate= d.pickup_date
            wh1 = d.warehouse_staff_status
            indianTeamstatus = d.status 
            AsnStatus = d.asn_sent
            WareHouseStaffStatus = d.warehouse_staff_status
            InvoiceStatus = d.invoice_send            
        if request.method == 'POST':
            remark2= request.POST.get('Warehouse_remark')                
            if remark2:
                remark_comment=remark_tbl(remark_po=Order.objects.get(po=instance.po), user = request.user, comment=remark2)
                remark_comment.save()        
        status = request.POST.get("STATUS")        
        wh_status = request.POST.get("wharehouse_staff_status")        
        invoice_send = request.POST.get("invoice_send")
        if form.is_valid():
            print ("close working")
            if "CLOSE" in wh1:                
                if PickUpDate:
                    if "YES" in asn_sent:                      
                        if "OPEN" in d.status or "NA" in d.status:                            
                            instance=form.save(commit=False)
                            instance.user = request.user                                
                            instance.save()
                            if str(d.company_name) == "Shopify":
                                OrderFullfill(request, instance = instance)
                            if str(d.company_name) == "eBay":
                                ebayfulfillOrder(request)
                            if str(d.company_name) == "Amazon":
                                fulfillOrderAmazon(request)
                            messages.success(request, "Successfully closed")
                            return HttpResponseRedirect(reverse('index'))                            
                        else:
                            messages.success(request, "allready Status closed")
                    else:
                        messages.success(request, "asn should be yes")
                else:
                    messages.success(request, "Please put PickUp Date")                                  
            else:
                messages.success(request, "First close the whareHouse Status")
            return HttpResponseRedirect(reverse('index'))
        else:
            print ("form not working")
    context = {
         "instance" :instance, # use this on template folder if you are extending form
         "page_title" : "Edit Page",
         "form":form,
         'indianTeamstatus':indianTeamstatus,
         "AsnStatus":AsnStatus,
         "WareHouseStaffStatus": WareHouseStaffStatus,
         "InvoiceStatus":InvoiceStatus,
         "group1":group1,
         "wh1": wh1,
       
    }
 
    return render(request, "ordermanage/edit.html" , context)

    
'''=============================================
   Main Dasbord Display 
   Project Name: OrderManage
   Name: OrderManage View
   URL: https://eknous.com
   Author: Eknous Technology Solutions
   License: https://eknous.com/license/
   function: SingleEdit 
   ============================================='''

@login_required(login_url="/")
def single_edit(request, pk=None):
    group1 = Group.objects.get(name="indianteam")
    """For Group Based Templates"""    
    if request.user in Group.objects.get(name="indianteam").user_set.all():
        group1 = "indianteam"
    elif request.user.is_superuser:
        group1 = "superuser"
    else:
        group1 = "warehouse"
    """End Group Based Templates"""

    group = Group.objects.get(name="warehouse").user_set.all()   
    if request.user in group and request.user.is_active:        
        return active_edit(request)
    else:        
        instance = get_object_or_404(Order, pk=pk)
        remark_tbl= remark
        form = SingleEditForm(request.POST or None, instance=instance)
        if request.method == "POST":
            remark2= request.POST.get('Warehouse_remark')                
            if remark2:
                remark_comment=remark_tbl(remark_po=Order.objects.get(po=instance.po), user = request.user, comment=remark2)
                remark_comment.save()
            if form.is_valid():               
                wh_stat= Order.objects.filter(warehouse_staff_status=instance.warehouse_staff_status)               
                for wh in wh_stat:
                    wh_status = wh.warehouse_staff_status
                    status = wh.status
                    print (status)
                if "OPEN" in wh_status or "NA" in wh_status:                            
                    instance=form.save(commit=False)                  
                    instance.user = request.user
                    if not "CLOSE" in instance.status : 
                        indian_status = Order.objects.get(po=instance.po)
                        if not 'YES' in instance.asn_sent and not 'YES' in instance.invoice_send:                        
                            if not "NA" in indian_status.status:             
                                instance.save()
                                return HttpResponseRedirect(reverse('index'))
                            else:
                                messages.success(request, "you can't edit! PO is disable")
                        else:
                            messages.success(request, "you can't edit! Warehouse Status is open ")
                    else:
                        messages.success(request, "You Can Not close Status")                                                
                else:
                    messages.success(request, "WareHouse Status is already Closed You Can't Edit")
    context = {
        'form': form,
        'instance':instance,
        'group1': group1

    }

    return render(request, "ordermanage/update.html" , context)



'''=============================================
   Main Dasbord Display 
   Project Name: OrderManage
   Name: OrderManage View
   URL: https://eknous.com
   Author: Eknous Technology Solutions
   License: https://eknous.com/license/
   ============================================='''


@login_required(login_url="/")
def index(request):
    """For Group Based Templates"""   
    group1 = chek_group(request.user)    
    """End Group Based Templates"""          
    companys = Company.objects.all()    
    total = Product.objects.all().count()
    red = Product.objects.filter(available_quentity__lt = 20).count()      
    yellow = Product.objects.filter(available_quentity__range =(20, 100)).count()
    remain = total -(red + yellow )    
    red_percent = red*100/total        
    yellow_percent = yellow*100/total        
    rest = 100 -(red_percent +yellow_percent)          
    query_results_order = Order.objects.order_by('-order_date')[:][::1]
    notification = remark.objects.order_by('-date')
    paginator = Paginator(query_results_order, 15)
    page = request.GET.get('page')
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        queryset = paginator.page(1)
    except EmptyPage:
        queryset = paginator.page(paginator.num_pages)
    query = request.GET.get("po")        
    if query:
        query_results_order= query_results_order.filter(po__icontains=query)
    
    try:
        warehouse_open = Order.objects.filter(warehouse_staff_status="OPEN").count()
    except Exception:
        print ("You have no open order")
    try:
        warehouse_close = Order.objects.filter(warehouse_staff_status="CLOSE").count()
    except Exception:
        print ("no close order")
    try:
        open = Order.objects.filter(status="OPEN").count()
    except Exception:
        print ("You have no open order")
    try:
        close = Order.objects.filter(status="CLOSE").count()
    except Exception:
        print ("no close order")
    try:
        neworder = Order.objects.filter(order_date=datetime.date.today()).count()
    except Exception:
        print ("no close order")
    try:
        twodays = Order.objects.filter(status="OPEN").filter(order_date__gte=timezone.now()-timedelta(days=2)).count()
        # print " twodays %s" % twodays
        
    except Exception:
        print ("no close twodays")
    try:
        threedays = Order.objects.filter(status="OPEN").filter(order_date__lte=timezone.now()-timedelta(days=3)).count()
        threeday = open-twodays
        # print threeday
        # print timezone.now()-timedelta(days=3)
    
    except Exception:
        print ("no close threedays")
    
    '''============Newline Start=============='''
    if request.method == 'POST':
        template_checkbox = request.POST.getlist('selectCheckbox')
        length =len(template_checkbox)
        
        
        for selected_pk in range(length):
            selected_pk=template_checkbox[selected_pk]                
            status_update=Order.objects.get(pk=selected_pk)            
            if status_update.pickup_date:
                if "OPEN" in status_update.status:
                    status_update.status="CLOSE"
                    status_update.asn_sent ="YES"
                    status_update.invoice_send ="SENT"
                    status_update.save()
                    messages.success(request, "update successfull")
                else:
                    messages.success(request, "status are already closed")
            else:
                    messages.success(request, "No PickUp Date")

    '''============end NEw line code=============='''

    #templets_name = "ordermanage/order.html"
    
    context = {
        "query_results_order" :query_results_order,
        "object_list" :queryset,       
        "open" :open,
        "close" : close,        
        "page_title" : "OrderManage MainPage" ,
        'warehouse_open': warehouse_open,
        "neworder":neworder,
        "twodays": twodays,
        "threedays": threedays,
        'notification': notification,
        'yellow_percent':yellow_percent,
        'red_percent':red_percent,
        'rest':rest,
        'red':red,
        'yellow':yellow,
        'remain':remain,
        'group1': group1,
        }

    return render(request, "ordermanage/order.html" , context)


'''============================================
       details Page Display for Main Table
       Project Name: OrderManage
       Name: OrderManage View
       URL: https://eknous.com
       Author: Eknous Technology Solutions
       License: https://eknous.com/license/ 
   ============================================'''

@login_required(login_url="/")
def details(request, pk=None):
    """For Group Based Templates"""
    group1 = chek_group(request.user)
    """End Group Based Templates"""   
    order_details = get_object_or_404(Order, pk=pk)
    order_quantity = LineTable.objects.filter(purchase_order=pk)
    remark2 = remark.objects.filter(remark_po=pk).order_by('-date')[:1]
    notification = remark.objects.filter(remark_po=pk).order_by("-date")
    remark_tbl= remark
    if request.method == 'POST':
        remark3= request.POST.get('Warehouse_remark')       
        if not remark3.isspace() and remark3:
            remark_comment=remark_tbl(remark_po=Order.objects.get(po=order_details.po), user = request.user, comment=remark3)
            remark_comment.save()
            return HttpResponseRedirect(reverse('index'))
        else:
                messages.success(request, "Remark field is empty ") 

    context = {
        "order_details" :order_details,
        "order_quantity":order_quantity ,
        "remark2":remark2 ,
        'notification': notification,
        "page_title" : order_details.pk ,
        "group1":group1
    }
    return render(request, "ordermanage/details.html", context)


'''======================================
   create a new records using templet view
   ======================================'''


@login_required(login_url="/")
def create(request):
    """For Group Based Templates"""   
    group1 = chek_group(request.user)
    """End Group Based Templates"""    
    print(request.user)
    # print(group1)
    # print(group)
    if not request.user.is_superuser and not (request.user in group) and request.user.is_active:
        # raise PermissionDenied
        print('is superuser')
        return HttpResponseRedirect(reverse('404'))
    else:  
        courier_query = CourierMethod.objects.all()        
        query_order = Order.objects.all()
        count = 0
        if request.method == 'POST':    
            for i in query_order:
                PO=i.po
                if request.POST.get("po") == PO:
                    count=1
            print (count)
            if count == 1:
                messages.success(request, "Already exist")
        queryset_shop = Company.objects.all()
        if request.is_ajax():
            queryset_shop = Company.objects.filter(company_name = 1)        
        form = CreateForm(request.POST or None)
        address_form = AddressForm(request.POST or None)        
        temp1=0
        OrderDate = datetime.date.today()       
        finaldate = datetime.datetime.strptime(str(OrderDate), '%Y-%m-%d')        
        if form.is_valid():
            OrderPoDate = request.POST.get("order_po_date")
            finaldate = datetime.datetime.strptime(str(OrderDate), '%Y-%m-%d')
            finaldate2 = datetime.datetime.strptime(str(request.POST.get("order_po_date")), '%Y-%m-%d')
            if OrderPoDate:
                if finaldate >= finaldate2:
                    instance=form.save(commit=False)             
                    instance.user = request.user
                    instance.name = request.POST.get("company_name")
                    # instance.id = request.POST.get('po')
                    instance.save()
                    # return HttpResponseRedirect(reverse('index'))                   
                    obj_table= LineTable                
                    length = request.POST.getlist("new_Sku1")
                    quantity = request.POST.getlist("qty")                
                    for i, j in zip(range(len(length)), range(len(quantity))):                            
                        companysku=length[i]                                                 
                        quentity1=quantity[j]                                        
                        save_obj= obj_table(purchase_order = Order.objects.get(po=instance.po), vendor_SKU =companysku , order_quantity=quentity1)
                        save_obj.save() 
                        queryset_shop2 = Company.objects.filter(company_sku=companysku)                    
                        if queryset_shop2:
                            for query in queryset_shop2:
                                q1= query.Product_SKU.quantity #100   
                                q2= query.Product_SKU.available_quentity                  
                        block_quantity=0                        
                        block_quantity = block_quantity + int(quentity1)
                        temp = q1 - int(quentity1) # need to save q so that it will update inventry quentity
                        temp1 = q1 - int(quentity1)
                        shop_instance = Product.objects.filter(id = query.Product_SKU.id)
                        print (shop_instance)
                        if q2 >= block_quantity:
                            instance.flag=0 # green
                            instance.save()
                        elif q2< block_quantity:
                            instance.flag=1 #partical orange
                            instance.save()
                        else:
                            instance.flag=2 # Red 
                            instance.save()
                        for shop_instance in shop_instance:
                            shop_instance.block_quantity= block_quantity
                            shop_instance.available_quentity=temp
                            shop_instance.save()
                            block_quantity = shop_instance.block_quantity
                            messages.success(request, "Block Quantity: %s" % block_quantity)
                            if temp <=0 :
                                temp=temp-temp-temp
                                messages.success(request, "Availble Quantity is less than order Quantity : %s, NEED MORE %s  " % (companysku, temp))
                            else:
                                messages.success(request, "Available Quantity in inventry: %s" % temp)
                                print (block_quantity)
                    # obj_table= LineTable
                    
                    remark_tbl = remark                
                    comment = instance.remark 
                    if not comment.isspace() and comment:
                        remark_comment= remark_tbl(remark_po=Order.objects.get(po=instance.po), user = instance.user, comment=instance.remark)
                        remark_comment.save()
                    # address_tbl = Address
                    save_address= address_form.save(commit=False)
                    save_address.po_number = Order.objects.get(po=instance.po)
                    save_address.customer_name= instance.user
                                   
                    save_address.save()                                                   
                else:
                    messages.success(request, "Created Date is Wrong")        
        else:
            print ("address not valide")   
    context = {
        "form":form,
        "address_form":address_form,
        "temp1":temp1,        
        "queryset_shop":queryset_shop,
        "query_order" :query_order,
        "courier_query":courier_query,
        "group1":group1,       
        }    
    return render(request, "ordermanage/create.html" , context)

'''====================================
    Update Status
   ===================================='''


@login_required(login_url="/")
def update_status(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('404'))
    query_results_order = Order.objects.orderby('-order_date')[:][::1]   
    context = {
        "query_results_order" :query_results_order,
       
        "page_title" : "Order" ,
    }
    return render(request, 'ordermanage/update_status.html'  , context)

'''=====================================
       Staff Details Page Display
   ====================================='''


  

def OpenOrder(request):
    open = Order.objects.filter(status="open")
    query = request.GET.get("po")
    if query:
        open= open.filter(po__icontains=query)
    context = {
        "open" : open,
        "page_title" : "open_order",
    }

    return render(request, "ordermanage/staff_details.html", context)



'''====================================
     warehouse Team Edit Page display
   ===================================='''


@login_required(login_url="/")
def active_edit(request, pk= None):    
    """For Group Based Templates"""
    group1 = chek_group(request.user)
    """End Group Based Templates"""
    queryset = Order.objects.filter(po= pk)
    instance = get_object_or_404(Order, pk=pk)   
    wh_status = instance.warehouse_staff_status
    remark_tbl= remark
    if request.method == 'POST':
        remark2= request.POST.get('Warehouse_remark')        
        if remark2 and not (wh_status == 'close' or wh_status == 'CLOSE'):
            remark_comment=remark_tbl(remark_po=Order.objects.get(po=instance.po), user = request.user, comment=remark2)
            remark_comment.save()    
    PickupDate = request.POST.get("pickup_date")
    OrderDate = Order.objects.get(po=instance.po)
    
    form = ActiveUserForm(request.POST or None, instance = instance)   
    if request.method == 'POST':        
        if form.is_valid():            
            if PickupDate:                
                if "OPEN" in OrderDate.warehouse_staff_status:
                    # print (OrderDate.warehouse_staff_status)
                    # print (instance.warehouse_staff_status)
                    if "OPEN" in OrderDate.status:
                        finaldate = datetime.datetime.strptime(str(OrderDate.order_date), '%Y-%m-%d')
                        finaldate2 = datetime.datetime.strptime(str(PickupDate), '%Y-%m-%d')
                        if finaldate2 >= finaldate:  
                            instance = form.save(commit = False)
                            print (instance)               
                            instance.pickup_date = PickupDate
                            instance.warehouse_staff_status = "CLOSE"
                            instance.save()
                            print ("=============================")
                            x = OrderDate.company_name
                            print (x)
                            print (type(x))                              
                            return HttpResponseRedirect(reverse('index'))
                        else:
                            messages.success(request, "not valid date in else")
                    else:
                        messages.success(request, "po id Disable")
                else:
                        messages.success(request, "status is already closed")
            else:
                messages.success(request, "Please Put pickUp date")
                
            #         messages.success(request, "Pickup date MUST be greterthan Order date ")
            # else:
            #     messages.success(request, "Please Put pickUp date") 
        

    return render(request, "ordermanage/warehouseedit.html", {"form":form, "instance" :instance, "group1":group1})


'''====================================
####shopify app url
===================================='''


@login_required(login_url="/")
def welcome(request):
    return render('home/welcome.html', {
        'callback_url': "http://%s/login/finalize" % (request.get_host()),
    })
    

'''====================================
   For Test Purpose    
===================================='''    

def errorpage(request):
    
    return render(request,"404.html", {"user":request.user,})


'''====================================
Operation open order
===================================='''


@login_required(login_url="/")
def OperationOpenorder(request):
    """For Group Based Templates"""
    group1 = chek_group(request.user)
    """End Group Based Templates"""
    open = Order.objects.filter(status="OPEN")
    context = {
        "open":open,
        "group1":group1,

    }   
    return render(request, 'ordermanage/operation_open_order.html', context)


'''====================================
   WareHouse Open Order
===================================='''


@login_required(login_url="/")
def WhOpenorder(request):
    """For Group Based Templates"""
    group1 = chek_group(request.user)
    """End Group Based Templates"""    

    warehouse_open = Order.objects.filter(warehouse_staff_status="OPEN")
    context = {
        "open":warehouse_open,
        "group1":group1,

    }   
    return render(request, 'ordermanage/warehouse_open_order.html', context)

'''====================================
Open Order Greater than 2 days
===================================='''


@login_required(login_url="/")
def TwodaysOpen(request):
    """For Group Based Templates"""
    group1 = chek_group(request.user)
    """End Group Based Templates"""
    twodays = Order.objects.filter(status="OPEN").filter(order_date__gte=timezone.now()-timedelta(days=2))
    context = {
        "open":twodays,
        "group1":group1,

    }   
    return render(request, 'ordermanage/twodaysopen.html', context)


'''====================================
Open Order Greater than 3 days
===================================='''


@login_required(login_url="/")
def ThreedaysOpen(request):
    group1 = chek_group(request.user)
    """End Group Based Templates"""   
    threedays = Order.objects.filter(status="OPEN").filter(order_date__lte=timezone.now()-timedelta(days=3))
    context = {
        "open":threedays,
        "group1":group1,

    }   
    return render(request, 'ordermanage/threedaysopen.html', context) 


'''==================================
Add Courier
=================================='''


def couriermethod(request):
    group1 = chek_group(request.user)
    """End Group Based Templates"""
    form =CourierMethodForm(request.POST or None)
    all_couriermethod = CourierMethod.objects.all()
    if request.method == 'POST':
        if form.is_valid:
            instance = form.save(commit = False)
            instance.save()
            messages.success(request, "Successfully Added")
            return HttpResponseRedirect(reverse('courier-method'))
    else:
        form = CourierMethodForm()

    context = {'form':form,
            'all_couriermethod':all_couriermethod,
            'group1':group1
             }
    return render(request, 'ordermanage/couriermethod.html', context)
'''================================== 
# company delete 
==================================='''

def couriermethod_delete(request ,id= None):
    instance = get_object_or_404(CourierMethod, id=id)
    if request.method == 'POST':
        instance.delete()
        messages.success(request, "Successfully Deleted")
    return render(request ,'index',{} )


##########################################################
#=============shopify Request ============================
##########################################################


def ReciveOrder(request):
    if request.method == 'GET':
        API_KEY = 'a5aab5ac3960fc8a59f975825ab6b759'
        PASSWORD = 'adc4aed5302ff9b07e10d5f864806d4a'
        shop_url = "https://ordermanage.myshopify.com/admin/orders.json"        
        response = requests.get(shop_url ,verify=False, auth=(API_KEY, PASSWORD) )
        print ("=========> shopify response")
        data = json.loads(response.content)       
        order_request = GetOrder
        
        shopify_order = Order       # Order(main table )Objects     
        for order in data["orders"]:            
            print (order["id"])
            po =order["id"]
            print (type(po))
            dd= order["created_at"]
            dateOrd = str(dd).split("T")
            print (dateOrd)
            orderdate= datetime.datetime.strptime(dateOrd[0], '%Y-%m-%d')
            print (orderdate)
            print (type(orderdate))
            id = order['customer']['id']
            name = order['customer']['first_name']
            contact_number =order['customer']['phone']
            shopify_recive_order = shopify_order(po=str(po) ,user =User.objects.get(id=2) ,company_name=Company.objects.get(id=3) ,courier_method=CourierMethod.objects.get(id=1), order_date= orderdate)
            shopify_recive_order.save()            
            order_recive = order_request(customer_id=id ,customer_name= name, contact_number =contact_number  )
            order_recive.save()
        return HttpResponse("success")


##########################################################
#=============shopify OrderFullfill=======================
##########################################################


def OrderFullfill(request, instance):
    query_set = Order.objects.all().filter(company_name =3)
    query_set2 = Order.objects.filter(po = instance)
    for i in query_set2:
        po = i.po
        tracking_number = i.traking_number
        print (po)          
        API_KEY = 'a5aab5ac3960fc8a59f975825ab6b759'
        PASSWORD = 'adc4aed5302ff9b07e10d5f864806d4a'
        #shop_url = "https://ordermanage.myshopify.com/admin/orders/805312987194/fulfillments.json"    
        headers  = {'Content-type':'application/json'}
        datas = {
                    "fulfillment": {
                        "order_id":instance.po,
                        "tracking_number": tracking_number,
                        "tracking_urls": [
                            "https://shipping.xyz/track.php?num="+tracking_number,
                            "https://anothershipper.corp/track.php?code=abc"
                        ],
                        "notify_customer": True                  
                }
            }   
        response = requests.post('https://ordermanage.myshopify.com/admin/orders/'+po+ '/fulfillments.json', headers =headers, auth=(API_KEY, PASSWORD ), verify=True, data= json.dumps(datas))

    return index(request)

def CreateInventory(request):
    API_KEY = 'a5aab5ac3960fc8a59f975825ab6b759'
    PASSWORD = 'adc4aed5302ff9b07e10d5f864806d4a'
    headers  = {'Content-type':'application/json'}
    shop_url = "https://ordermanage.myshopify.com/admin/inventory_items.json"
    datas = {
                "inventory_item": { 
                                    'id':1942458302524,     
                                    "sku": "001 new sku",
                                  }
            }
    response = requests.put("https://ordermanage.myshopify.com/admin/inventory_items.json" , headers =headers, auth=(API_KEY, PASSWORD ), verify=True, data= json.dumps(datas))
    # response = requests.put("https://ordermanage.myshopify.com/admin/inventory_items.json" , data=datas)
    
    print (response)
    return HttpResponse(response) 

# def CreateProduct(request):
#     # API_KEY = 'a5aab5ac3960fc8a59f975825ab6b759'
#     # PASSWORD = 'adc4aed5302ff9b07e10d5f864806d4a'
   
#     headers  = {'Content-type':'application/json',
#                 'X-Shopify-Access-Token':'72b3bee1ce7aae5a958f6b5db3eba5a3',
                
#                 }

#     datas = {
#                 "product": {
#                                 "title": "new product by ordermanage",
#                                 "body_html": "<strong>Good snowboard!</strong>",
#                                 "vendor": "ordermanage",
#                                 "product_type": "ordermanage",
#                                 "tags": "bedsheets, Bedsheets;"
#                             }
#                 }

#     # request.put( "https://ordermanage.myshopify.com/admin/inventory_items.json", auth=(API_KEY, PASSWORD ), verify=True, data= json.dumps(datas) )
#     # response = requests.put("https://ordermanage.myshopify.com/admin/inventory_items.json" , headers =headers, auth=(API_KEY, PASSWORD ), verify=True, data= json.dumps(datas))
   
#     response= requests.post("https://ordermanage.myshopify.com/admin/products.json", headers =headers, auth=(headers), verify = True, data= json.dumps(datas), )
#     print (response)
#     return HttpResponse(response)



# def ShopifyLogin(request):
#     api_key = 'ead441fdcf80b836985b08647f6119c0'
#     api_secret_key= '8e243f0dd108f7402056831f5d72c22d'
#     scope =str(['read_products'])    
#     response = "https://ordermanage.myshopify.com/admin/oauth/authorize?client_id="+api_key +'&scope=write_products,read_orders,write_orders' +'&redirect_uri='+'http://localhost:8000/ordermanage/catch'+'&state=12345'
#     return HttpResponseRedirect(response)

# def ShopifyRedirect(request):
#     api_key = 'ead441fdcf80b836985b08647f6119c0'
#     api_secret_key= '8e243f0dd108f7402056831f5d72c22d'
#     getresponse  = request.GET.get('shop')
#     print (getresponse)   
#     code= str(request.GET.get('code'))    
#     session = shopify.Session('shop')
#     timestamp= request.GET.get('timestamp')
#     hmac = request.GET.get('hmac')
#     headers  = {'Content-type':'application/json'}   
#     datas = {        
#         'client_id':api_key,
#         'client_secret':api_secret_key,
#         'code':code,
#         }
    
#     url =requests.post('https://ordermanage.myshopify.com/admin/oauth/access_token', data=datas)
#     a=  json.loads(url.content)
#     print (a) 
#     query_accesstoken = ShopifyAccessToken
#     print (a['access_token'])
#     if a:
#         instance = query_accesstoken(shopify_shop=getresponse ,access_token =a['access_token'], scope = a['scope'] )
#         instance.save()    
#     return redirect('index')


def products(request):    
    headers ={
        'Content-Type':'application/json',
        'X-Shopify-Access-Token':'72b3bee1ce7aae5a958f6b5db3eba5a3',       
    }
    datas = {
                "X-Shopify-Access-Token":"72b3bee1ce7aae5a958f6b5db3eba5a3",
                "product": {                                
                                "title": "autkey1",
                                "body_html": "<strong>Good snowboard!</strong>",
                                "vendor": "ordermanage",
                                "product_type": "ordermanage",
                                "tags": "bedsheets, Bedsheets;"
                            }
                }
    response= requests.post("https://ordermanage.myshopify.com/admin/products.json", data=json.dumps(datas), headers=headers)    
    print (response)
    return redirect('index')


"""====================================================
create Invetory With access Token 
====================================================="""
def CreatePublicInventory(request):
    headers ={
        'Content-Type':'application/json',
        'X-Shopify-Access-Token':'72b3bee1ce7aae5a958f6b5db3eba5a3',       
    }
    shop_url = "https://ordermanage.myshopify.com/admin/inventory_items.json"
    datas = {
                "inventory_item": { 
                                    'id':1942458302527,     
                                    "sku": "001 new sku",
                                  }
            }
    response = requests.post("https://ordermanage.myshopify.com/admin/inventory_items.json" , data= json.dumps(datas), headers =headers,)
    # response = requests.put("https://ordermanage.myshopify.com/admin/inventory_items.json" , data=datas)
    
    print (response)
    return HttpResponse(response)

"""====================================================
Order Fullfillment With access Token 
====================================================="""

def OrderPublicFullfill(request):
    headers ={
        'Content-Type':'application/json',
        'X-Shopify-Access-Token':'72b3bee1ce7aae5a958f6b5db3eba5a3',       
    }
    query_set = Order.objects.all().filter(company_name =3)
    for i in query_set:
        po = i.po
        print (po)
    datas = {
                "fulfillment": {
                    "order_id":po,                    
            }
        }   
    response = requests.post('https://ordermanage.myshopify.com/admin/orders/'+po+ '/fulfillments.json', headers =headers, data= json.dumps(datas))

    return HttpResponse(response)

'''==========================================
   ==============UPS INtegration ============
   =========================================='''
