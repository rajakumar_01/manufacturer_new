# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-11-21 09:46
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ordermanage', '0021_auto_20181101_1544'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='company_name',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Company_company_name', to='shop.CompanyName'),
        ),
    ]
