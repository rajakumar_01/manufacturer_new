import React,{Component} from "react";
import {Col, Row} from "react-bootstrap";
import { withRouter } from "react-router";
import {Link} from 'react-router-dom'
import './style/Dashboard.css'
import logo from './media/logo_01.png'

class SidebarManufacturer extends Component{
    constructor(props){
        super(props)
        this.state={
            dashboard:false,
            shopify:false,
            manufacturer:false,
            inventory:false,
        }
        this.onclickdashboard = this.onclickdashboard.bind(this)
        this.onclickInventory = this.onclickInventory.bind(this)
        this.onclickManufacturer = this.onclickManufacturer.bind(this)
        this.onclickshopify = this.onclickshopify.bind(this)
        this.onleave = this.onleave.bind(this)
        this.insideRowStyle={fontSize:20,marginLeft:30,marginTop:10}
        this.styleLink= {color:"white"}
    }

    onclickdashboard(){
        this.setState(prevstate => {
            return{dashboard: !prevstate.dashboard}
          })
    }
    onclickInventory(){
        this.setState(prevstate => {
            return{inventory: !prevstate.inventory}
          })
    }
    onclickManufacturer(){
        this.setState(prevstate => {
            return{manufacturer: !prevstate.manufacturer}
          })
    }
    onclickshopify(){
        this.setState(prevstate => {
            return{shopify: !prevstate.shopify}
          })
    }
    onleave(){
        this.setState({shopify:false,
                        manufacturer:false,
                        inventory:false})
    }

    render(){
        
        return(
            <div className="Navbar">
                
                    <Col>
                        <div className="logonav">
                        <Row>
                            <img src={logo}/>   
                        </Row>
                        </div>
                    </Col>
                
                <br/>
                <Col>
                    <Row>
                    <span className="material-icons" style={{marginRight:10}}>&#xe7fd;</span>                    
                    <h3>{this.props.user}</h3>
                    </Row>
                </Col>
                <hr />
                <Col onPointerEnter={this.onclickdashboard} onPointerLeave={this.onleave}>
                <Link to={'/signuping'} style={this.styleLink}>
                <Row>
                    <span className="material-icons" style={{marginRight:10}}>&#xe7fd;</span>                    
                    <h3>Dashboard</h3>
                    
                </Row>
                </Link>
                </Col>
                <hr />
                <Col onPointerEnter={this.onclickInventory} onPointerLeave={this.onleave}>
                    <Row><span className="material-icons" style={{marginRight:10}}>&#xe53b;</span>
                    <h3>Inventory</h3>
                    </Row>
                    {this.state.inventory ?<div> 
                        <Link to={'/signuping'} style={this.styleLink} ><Row  style={this.insideRowStyle}>Inventory Dashboard</Row></Link>
                        </div> : <div></div>}
                </Col>
                <hr />
                <Col onPointerEnter={this.onclickManufacturer} onPointerLeave={this.onleave}>
                <Row><span className="material-icons"  style={{marginRight:10}}>&#xe53b;</span>
                    <h3>Manufacturer</h3>
                    </Row>
                    {this.state.manufacturer ?<div> 
                        <Link to={'/signuping'} style={this.styleLink} ><Row style={this.insideRowStyle}>Manufacturer</Row></Link>
                        <Link to={'/signuping'} style={this.styleLink} ><Row style={this.insideRowStyle}>Register</Row></Link>


                        </div> : <div></div>}
                </Col>
                <hr />
                <Col>
                <Link to='/logout' style={this.styleLink}>
                <Row>
                    <span className="material-icons" style={{marginRight:10}}>&#xe8ac;</span>

                    <h3>Logout</h3>
                    </Row>
                    </Link>
                </Col>
            </div>
            )        

        }
        
}


export default SidebarManufacturer