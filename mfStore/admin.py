# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(AddManufacturer)
admin.site.register(Address)
admin.site.register(ManufacturerMapping)


class CreateManufacturerOrderAdmin(admin.ModelAdmin):
    model = CreateManufacturerOrder
    list_display = ["order_by", "order_for",
                    "manufacturer_list", "order_date", ]


admin.site.register(CreateManufacturerOrder, CreateManufacturerOrderAdmin)


class workProgreassBarAdmin(admin.ModelAdmin):
    model = WorkProgressBar
    list_display = ["lineitem",
                    "step_position",
                    "step_id",
                    "step_description",
                    "location",
                    "date",
                    "Final_date"]


admin.site.register(WorkProgressBar, workProgreassBarAdmin)


class ManufactureLineTableForm(admin.ModelAdmin):
    model = ManufactureLineTable
    list_display = ["manufacturer_table",
                    "item_sku",
                    "item_name",
                    "dimention",
                    "color",
                    "quantity",
                    "pack_size",
                    "delivery_date",
                    "Eta",
                    "mf_status"]


admin.site.register(ManufactureLineTable, ManufactureLineTableForm)
admin.site.register(remark)
