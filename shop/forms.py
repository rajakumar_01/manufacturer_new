from django import forms
from django.forms import MultiWidget
from .models import *

#FOR DISPLAYING DETAILS
class SimpleForm(forms.ModelForm):

    class Meta:
        model = Variant
        fields = ["variant_sku","variant_image" ,"title", "inventory_item_quantity"]




# class CreateForm(forms.ModelForm):

#     class Meta:
#         model = Product
#         fields = ["Product_SKU", "category", "image", "name", "quantity", "price", "bulletpoints", "description", "available_quentity", "Product_Dimensions", "Color", "ASIN", "Material_Type", ]
#         # fields = ["Product_SKU","image" ,"name","Product_id", "quantity","price","category"]

class CreateCategory(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['name']
        
class AddCompanySkuForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = ['Product_SKU', "company_sku", 'company_name']  
class AddCompanyForm (forms.ModelForm):
    class Meta:
        model = CompanyName
        fields = ['companyname']
    

