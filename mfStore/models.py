# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from shop.models import *
import random
from django.core.validators import RegexValidator
import datetime

# Create your models here.

# >>>>>> for rendom number


def random_text():  # not in use
    text = 'WH_PO'
    val = random.randint(100, 1000)
    output = '{}-{}'.format(text, val)
    return output


OrderStatus = (('open', 'Open'),
               ('close', 'Close'),
               ('na', 'NA'))


Stepprogress = (
    ('1', 'Out-from-MFStore'),
    ("2", "Dispatch for warehouse"),
    ("3", "On The Ship"),
    ("4", "Reached Destination"),
    ("5", "Deliverd")

)

status = (
    ("Pending", "Pending"),
    ('Acknowledge', 'Acknowledge'),
    ("Completed", "Completed"),
)


"""==============================="""
""">>>>>Create Manufacturer Table """
"""==============================="""


class AddManufacturer(models.Model):
    # manufacturer_name = models.ForeignKey(
    #     User, on_delete=models.CASCADE, related_name='manufacturer_name')
    manufacturer_name = models.CharField(
        max_length=255, verbose_name='Manufacturer Name')
    # email = models.EmailField(max_length=70, blank=True)
    # password =  models.CharField(max_length =20, blank =true)
    manufacturer_loction = models.CharField(max_length=50, null=False)

    def __str__(self):
        return str(self.manufacturer_name)


"""======================================="""
""">>>>>Create Manufacturer Mapping Table """
"""======================================="""


class ManufacturerMapping(models.Model):
    Product_SKU = models.ForeignKey(Variant, related_name='Mf_Sku', on_delete=models.CASCADE)
    Manufacturer_SKU = models.CharField(max_length=255, unique=True)
    # company_name = models.ForeignKey(
    #     CompanyName, related_name='Manufacturer_Company')
    manufacturer = models.ForeignKey(
        AddManufacturer, related_name='Manufacturer_mapping', on_delete= models.CASCADE)

    def __str__(self):
        return str(self.Manufacturer_SKU)


"""====================================="""
""">>>>>Create Manufacturer Order Table """
"""====================================="""


class CreateManufacturerOrder(models.Model):
    invoice_no = models.CharField(
        default=random_text, max_length=20, unique=True, verbose_name="PO-Number")
    order_by = models.CharField(
        max_length=256, null=True)  # may be fk in future
    order_for = models.ForeignKey(
        Company, related_name='CompanyForOrder', on_delete=models.CASCADE)
    manufacturer_list = models.ForeignKey(
        AddManufacturer, related_name='ManufacturerNameList', on_delete=models.CASCADE)
    order_date = models.DateField(auto_now_add=True, blank=False, null=False)
    step_position = models.CharField(
        max_length=200, choices=Stepprogress, default="1")
    order_status = models.CharField(
        max_length=10, choices=OrderStatus, default="open")

    # brand = models.CharField(max_length=256, null=True)
    # sub_brand = models.CharField(max_length=256, null=True)
    # category = models.CharField(max_length=256, null=True)
    # type = models.CharField(max_length=256, null=True)
    # container = models.CharField(max_length=256, null=True)
    # multi_pack = models.CharField(max_length=256, null=True)
    # size = models.CharField(max_length=256, null=True)
    # order_to = models.CharField(max_length=256, null=True)  # choice field

    def __str__(self):
        return str(self.invoice_no)


"""===================================="""
""">>>>>Create Manufacturer Line Table """
"""===================================="""


class ManufactureLineTable(models.Model):
    manufacturer_table = models.ForeignKey(
        CreateManufacturerOrder, on_delete=models.CASCADE, related_name='linetable')
    item_sku = models.ForeignKey(
        ManufacturerMapping, on_delete=models.CASCADE, related_name='Manufacturar_linetable', null=True)
    item_name = models.CharField(max_length=255, null=True)
    dimention = models.CharField(
        max_length=255, null=True,)
    color = models.CharField(max_length=255, null=True)
    quantity = models.CharField(max_length=25, null=True)
    pack_size = models.IntegerField()
    delivery_date = models.DateTimeField(blank=True, null=True)
    Eta = models.DateTimeField(blank=True, null=True)
    step_position = models.CharField(
        max_length=200, choices=Stepprogress, default="1")
    mf_status = models.CharField(
        max_length=225, choices=status, default="Pending")
    warehouse_status = models.CharField(max_length=255, choices=status, default="Acknowledge")

    def __str__(self):
        return str(self.item_name)


"""===================================="""
""">>>>>>>>>> Create Address >>>>>>>>>>"""
"""===================================="""


class Address(models.Model):
    manufacturer_name = models.ForeignKey(
        CreateManufacturerOrder,
        on_delete=models.CASCADE,
        related_name="manufacturer_location")
    address = models.CharField(max_length=256, null=True)
    city = models.CharField(max_length=256, null=True)
    state = models.CharField(max_length=256, null=True)
    zip = models.CharField(max_length=15, null=True)
    contact_number = models.CharField(max_length=15, null=True)

    def __str__(self):
        return str(self.manufacturer_name)


"""===================================="""
""">>>>>>>>>>==== Remark ====>>>>>>>>>>"""
"""===================================="""


class remark(models.Model):
    wh_remark = models.ForeignKey(
        CreateManufacturerOrder, related_name="newupdate", verbose_name='UpdatedBy', on_delete=models.CASCADE)
    comment = models.CharField(max_length=500, verbose_name='Remark')
    date = models.DateTimeField(
        auto_now_add=True, auto_now=False)
    comment_by = models.ForeignKey(User, related_name='user_comment', on_delete=models.CASCADE)

    def __str__(self):
        return (self.comment)


"""===================================="""
""">>>>>==== Step ProgressBar====>>>>>>"""
"""===================================="""
Stepprogress = (
    ('1', 'Out-from-MFStore'),
    ("2", "Dispatch for warehouse"),
    ("3", "On The Ship"),
    ("4", "Reached Destination"),
    ("5", "Deliverd")
)


class WorkProgressBar(models.Model):
    # wh_address = models.ForeignKey(
    #     Address, on_delete=models.CASCADE,)
    lineitem = models.ForeignKey(
        ManufactureLineTable, on_delete=models.CASCADE)
    step_position = models.CharField(
        max_length=200, choices=Stepprogress, default="1")
    step_id = models.IntegerField(default=None,)
    step_description = models.TextField(max_length=255)
    location = models.CharField(max_length=255)
    date = models.DateField(auto_now_add=True, blank=True, null=True)
    Final_date = models.DateField(blank=True, null=True)

    def __str__(self):
        return (self.step_position)
