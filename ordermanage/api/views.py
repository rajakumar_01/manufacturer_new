from rest_framework.generics import ListAPIView
from ordermanage.models import Order

class OrderListAPIView(ListAPIView):
    queryset= Order.objects.all()
