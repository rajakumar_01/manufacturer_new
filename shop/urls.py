from django.urls import path
from . import views 

app_name='shop'

urlpatterns = [
    # url(r'^$', views.product_list, name='product_list'),
    # url(r'^(?P<category_slug>[-\w]+)/$', views.product_list, name='product_list_by_category'),
    # url(r'^(?P<id>\d+)/edit/$', views.edit, name="edit"),
    # url(r'^(?P<id>\d+)/delete/$', views.product_delete, name="delete"),
    # #url(r'^/create/$', views.create, name="create"),
    # url(r'^product/create/$', views.create, name="create"),
    # url(r'^(?P<id>\d+)/(?P<slug>[-\w]+)/$', views.product_detail, name='product_detail'),
    # url(r'^product/company_stock/$', views.company_stock, name='company_stock'),
    # url(r'^product/category/$', views.category, name='category'),
    # url(r'^product/addcompany/$', views.AddCompanySku, name='addcompany'),
    # url(r'^product/company/$', views.AddCompany, name='company'),
    # url(r'^product/hundred/$', views.inventory_hundred_Percent, name='hundred'),
    # url(r'^product/lesthantwenty/$', views.inventory_twenty, name='lesthantwenty'),
    # url(r'^product/inventory_inrange/$', views.inventory_inrange, name='inventory_inrange'),
    # url(r'^product/create_product/$', views.CreateProduct, name='create_product'),
    # url(r'^product/shopifyGetProduct/$', views.shopifyGetProduct, name='shopifyGetProduct')
    # url(r'^company_stock/$', views.company_stock, name='company_stock'), 

    path('',views.product_list, name='product_list'),
    path('<slug:category_slug>/', views.product_list,name='product_list_by_category'),
    path('<int:id>/edit/', views.edit, name='edit'),
    path('<int:id/delete/', views.product_delete, name='delete'),
    # path('create/',views.create, name='create')
    path('product/create/', views.create, name='create'),
    path('<int:id>/<slug:slug>/',views.product_detail, name='product_detail'),
    path('product/company_stock/',views.company_stock,name='company_stock'),
    path('product/category/',views.category, name='category'),
    path('product/addcompany/', views.AddCompanySku, name='addcompany'),
    path('product/company/',views.AddCompany, name='company'),
    path('product/hundred/', views.inventory_hundred_Percent, name='hundred'),
    path('product/lesthantwenty/', views.inventory_twenty, name='lesthantwenty'),
    path('product/inventory_inrange/', views.inventory_inrange, name='inventory_inrange'),
    path('product/create_product/', views.CreateProduct, name='create_product'),
    path('product/shopifyGetProduct/', views.shopifyGetProduct, name='shopifyGetProduct')
    # path('company_stock/',views.company_stock, name='company_stock')

]
