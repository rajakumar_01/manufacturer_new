from django.contrib import admin
from .models import ShopifyAccessToken,ShopifyProductDetails
# Register your models here.

class ShopifyAccessTokenAdmin(admin.ModelAdmin):
    model = ShopifyAccessToken
    list_display= ['user_name','shopify_shop','access_token', 'scope']

admin.site.register(ShopifyAccessToken, ShopifyAccessTokenAdmin)

class ShopifyProductDetailsAdmin(admin.ModelAdmin):
    list_display = ['product_sku','product_id','variants_id','options_id','images_id']
admin.site.register(ShopifyProductDetails, ShopifyProductDetailsAdmin)