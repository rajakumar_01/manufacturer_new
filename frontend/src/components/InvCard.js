import React,{Component} from 'react'
import { Card,CardDeck } from 'react-bootstrap';
import {Link} from 'react-router-dom'
class Invcard extends Component{
    constructor(){
        super()
    }
    componentDidMount(){
        
    }

    render(){
        return(
            <div>
            <CardDeck>
                <Card border="success">
                    <Card.Body>
                    <Card.Title style={{textAlign:'center'}}>Inventory more than 100</Card.Title>
                    </Card.Body>
                    <Card.Footer>
                    <small className="text-muted">Last updated 3 mins ago</small>
                    </Card.Footer>
                </Card>
                <Card border="warning">
                    <Card.Body>
                    <Card.Title style={{textAlign:'center'}}>Inventory more than 20</Card.Title>
                    </Card.Body>
                    <Card.Footer>
                    <small className="text-muted">Last updated 3 mins ago</small>
                    </Card.Footer>
                </Card>
                
                <Card border="danger">
                    <Card.Body>
                    <Card.Title style={{textAlign:'center'}}>Inventory less than 20</Card.Title>
                    </Card.Body>
                    <Card.Footer>
                    <small className="text-muted">Last updated 3 mins ago</small>
                    </Card.Footer>
                </Card>
                
                </CardDeck>
                </div>
        )
    }
}

export default Invcard