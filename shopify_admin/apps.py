from django.apps import AppConfig


class ShopifyAdminConfig(AppConfig):
    name = 'shopify_admin'
