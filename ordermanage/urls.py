from django.urls import path
from django.conf.urls.static import static 
from . import views
urlpatterns = [
    # url(r'^$', views.index, name="index"), #main dashbord   
    # url(r'^(?P<pk>[^/]+)/active/$', views.active_edit, name="active"), # warehouse    
    # url(r'^create/$', views.create, name="create"), # indian team &superuser
    # url(r'^404/$', views.errorpage, name="404"), #
    # url(r'^(?P<pk>[^/]+)/details', views.details, name="details"), # detail page for indian And admin    
    # url(r'^(?P<pk>[^/]+)/edit/$', views.edit, name="edit"),# for main edit
    # url(r'^(?P<pk>[^/]+)/update/$', views.single_edit, name="update"), 
    
    # url(r'^update-status/$', views.update_status, name="update-status"), #test
    # url(r'^open/$', views.OpenOrder, name="open"),
    
    # url(r'^openorder/$', views.OperationOpenorder, name="openorder"),
    # url(r'^whopenorder/$', views.WhOpenorder, name="whopenorder"),
    # url(r'^twodaysopen/$', views.TwodaysOpen, name="twodaysopen"),
    # url(r'^threedaysopen/$', views.ThreedaysOpen, name="threedaysopen"),
    # url(r'^courier-method/$', views.couriermethod, name="courier-method"),
    
    # url(r'^fullfill/$', views.OrderFullfill, name='fullfill'),
    # url(r'^inventory/$', views.CreateInventory, name='inventory'),
    # # url(r'^create_product/$', views.CreateProduct, name='create_product'),
    # # url(r'^token1/$', views.ShopifyLogin, name='token1'),
    # # url(r'^catch/$', views.ShopifyRedirect, name='catch'),
    # url(r'^createproduct/$', views.products, name='createproduct'),
    # url(r'^createinventory/$', views.CreatePublicInventory, name='createinventory'),
    # url(r'^Orderfullfill/$', views.OrderPublicFullfill, name='Orderfullfill') 

    # # url(r'^(?P<id>[^/]+)/update/$', views.single_edit, name="update"), 

    path('', views.index, name="index"),
    path('<int:pk>/active/', views.active_edit, name="active"),
    path('create/', views.create, name="create"),
    path('404/',views.errorpage, name='404'),
    path('<int:pk>/details/',views.details, name="details"), # detail page for indian And admin
    path('<int:pk>/edit/', views.edit, name="edit"),# for main edit
    path('<int:pk>/update/', views.single_edit, name="update"),
    
    path('update-status/', views.update_status, name="update-status"), #test
    path('open/', views.OpenOrder, name="open"),
    path('openorder/', views.OperationOpenorder, name="openorder"),
    path('whopenorder/', views.WhOpenorder, name="whopenorder"),
    path('twodaysopen/', views.TwodaysOpen, name="twodaysopen"),
    path('threedaysopen/', views.ThreedaysOpen, name="threedaysopen"),
    path('courier-method/', views.couriermethod, name="courier-method"),
    path('fullfill/', views.OrderFullfill, name='fullfill'),
    path('inventory/', views.CreateInventory, name='inventory'),
    # path('create_product/', views.CreateProduct, name='create_product'),
    # path('token1'/, views.ShopifyLogin, name='token1'),
    # path('catch/', views.ShopifyRedirect, name='catch'),
    path('createproduct/', views.products, name='createproduct'),
    path('createinventory/', views.CreatePublicInventory, name='createinventory'),
    path('Orderfullfill/', views.OrderPublicFullfill, name='Orderfullfill'), 

    # path('<int:id>/update/',views.single_edit, name="update")

]
 