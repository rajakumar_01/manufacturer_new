import React,{Component} from 'react'
import {Table, Container, Button, Card} from 'react-bootstrap'
import {BrowserRouter,Link,Switch,Route,Redirect} from 'react-router-dom'
import logo from './media/logo.png'
import './style/Dashboard.css'

class Login extends Component{
    constructor(){
        super()
        this.state = {
          username:"",
          password:"",
          redirect:false,
          code:"",
        }
        
        console.log("inside login")
        this.onUpdateHandler = this.onUpdateHandler.bind(this)
        this.OnClickHandler = this.OnClickHandler.bind(this)
      }
      
      onUpdateHandler(event){
          this.setState({[event.target.name]:event.target.value})
      }
      OnClickHandler(){
        const requestOptions = {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json',
                
            },
            body: JSON.stringify(this.state),
            
        }
        
        fetch(process.env.URL+'apitoken/', requestOptions)
            .then(response => response.json())
            .then(data => {this.setState({code:data.token,redirect:true})})
            .then()
            .then(console.log(this.state))
            

      }
    render() {
          const hh = {
                
              height:400,
              width:400,
              }
            const style2= {
                marginTop:150,
                textAlign:"center"
              }
          if(this.state.redirect){
            localStorage.setItem('token',this.state.code)
            console.log("helloooooooooooooooooooooooooo",localStorage.getItem('token'))
              return(<Redirect to={{pathname:"/home",state:{token:this.state.code}}}/>)
          }
          return(
            <div>
                
                <Container style={style2}>
                        <img src={ logo } alt="image"/>
                        <h2 style={{marginTop:50}}>Please, login into your account</h2>
                
                    <Container className="loginBox">
                        <input type='text' placeholder='username' name='username' onChange={this.onUpdateHandler} style={{marginTop:50}}/> <br/>
                        <br/>
                        <input type='password' placeholder='password' name='password' onChange={this.onUpdateHandler} /><br />
                        <br />
                        <Button type='button' style={this.button} onClick={this.OnClickHandler}>Login</Button>   
                        <span></span>

                            
                        <Link to={'/signuping'} style={{marginLeft:10}}>Create account</Link>
                        
                    </Container>
                </Container>
            </div>
          )
        }
}


export default Login