from django import forms
from django.forms import ModelForm, MultiWidget, ModelChoiceField
from .models import *
from shop.models import *
from django.contrib.admin.widgets import AdminDateWidget
from django.forms.fields import DateField
# from django.core import validators
from django.core.validators import RegexValidator


# class AddManufacturerForm(forms.ModelForm):
#     class Meta:
#         model = AddManufacturer
#         fields = ['manufacturer_name', 'manufacturer_loction']


class AddManufacturerForm(forms.ModelForm):
    password = forms.CharField(label='MF PassWord')
    email = forms.EmailField(label="Email")

    class Meta:
        model = AddManufacturer
        fields = ['manufacturer_name',
                  'manufacturer_loction',
                  ]

    def __init__(self, *args, **kwargs):
        super(AddManufacturerForm, self).__init__(*args, **kwargs)
        self.fields['manufacturer_name'].queryset = User.objects.filter(
            groups__name='manufacturer')


"""<<<<<<< === Manufacturer Maping Form === >>>>>>> """


class ManufacturerMappingForm(forms.ModelForm):
    class Meta:
        model = ManufacturerMapping
        fields = '__all__'


"""<<<<<<<<<<== create Manufacturer Order Form ==>>>>>>>>>>>>"""


class OrderRequestForm(forms.ModelForm):
    class Meta:
        model = CreateManufacturerOrder
        fields = [ "order_for", "manufacturer_list", "order_status"]


'''<<<<<<<<<<<<<<== Sku lineItem Form ==>>>>>>>>>>>>>>>>'''


class LineItemForm(forms.ModelForm):
    class Meta:
        model = ManufactureLineTable
        list_editable = ['quantity', 'delivery_date']
        fields = ['item_sku', 'item_name',
                  'dimention',
                  'color',
                  'quantity',
                  'delivery_date', ]
        widgets = {'manufacturer_table': forms.HiddenInput(),
                   'delivery_date': forms.DateInput(attrs={'type': 'date'}), }


class AddressForm(forms.ModelForm):
    class Meta:
        model = Address
        fields = '__all__'


class WorkProgressBarForm(forms.ModelForm):
    class Meta:
        model = WorkProgressBar
        fields = [
            "step_description",
            "location",
            "Final_date"]
        widgets = {'Final_date': forms.DateInput(attrs={'type': 'date'}), }

    # class EditOrderForm(forms.ModelForm):
    #     class Meta:
    #         model = CreateManufacturerOrder
    #         fields = [

    #         ]


class MfStatus(forms.ModelForm):
    class Meta:
        model = ManufactureLineTable
        fields = ['step_position']
