from django.db import models
from django.urls import reverse
from django.core.signals import request_finished
from django.db.models.signals import pre_save
from django.dispatch import receiver
# from ordermanage.models import Company1


#
class Category(models.Model): 
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True, unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'category'
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:product_list_by_category', args=[self.slug])
########################################################################################



class Products(models.Model):
    product_id = models.PositiveIntegerField(unique=True)#product id
    title = models.CharField(max_length=200)#name of product
    product_image = models.FileField(upload_to='products/%Y/%m/%d', blank=True)
    body_html = models.TextField()#contents of prodct
    vendor = models.CharField(max_length=200)#vendor name
    product_type = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    handel = models.CharField(max_length=100)#name

    def __str__(self):
        return self.title
    def get_absolute_url(self):
        return reverse('shop:product_detail', args=[self.product_id])

class Variant(models.Model):
    product_id = models.ForeignKey(Products,on_delete=models.CASCADE)
    variant_id = models.PositiveIntegerField(unique=True)
    title = models.CharField(max_length=200)#name of varient
    variant_image = models.FileField(upload_to='products/%Y/%m/%d', blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    variant_sku = models.CharField(max_length=100)
    slug = models.SlugField(max_length=200, db_index=True)
    compare_at_price = models.DecimalField(max_digits=10, decimal_places=2)
    inventory_item_id = models.PositiveIntegerField()
    inventory_item_quantity = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    grams = models.CharField(max_length=20)
    weight =  models.IntegerField(default=0)
    weight_unit = models.CharField(max_length=20)
    inventory_policy =  models.CharField(max_length=20)

    def __str__(self):
        return self.product_id.title+":"+self.title
    def get_absolute_url(self):
        return reverse('shop:product_detail', args=[self.variant_id,self.slug])
# ############################################################################################### 
# class Product(models.Model):#inventry table  
    
#     # Product_id = models.PositiveIntegerField(default=None, blank=True, null=True ) #remove True
#     Product_SKU = models.CharField(max_length=20, unique=True)
#     name = models.CharField(max_length=200, db_index=True) # name of the product
#     slug = models.SlugField(max_length=200, db_index=True,) # built beautiful url 
#     image = models.FileField(upload_to='products/%Y/%m/%d', blank=True)
#     description = models.TextField(blank=True)
#     bulletpoints = models.TextField(blank=True)
#     category = models.ForeignKey(Category, related_name='products', on_delete=models.CASCADE)
#     Product_Dimensions = models.CharField(max_length=20, blank = False ,null = False)
#     Color = models.CharField(blank = False ,null = False, max_length=20)
#     price = models.DecimalField(max_digits=10, decimal_places=2)
#     quantity = models.PositiveIntegerField()
#     block_quantity = models.IntegerField(default=0, blank=True, null=True)
#     available_quentity= models.IntegerField(default=1, blank=True, null=True)
#     created = models.DateTimeField(auto_now_add=True)
#     available = models.BooleanField(default=True)    
#     updated = models.DateTimeField(auto_now=True)    
#     Material_Type = models.CharField(blank = False ,null = False, max_length=40) #
#     Model_Number = models.CharField(blank = True ,null = True, max_length=40) #optional
#     ASIN = models.CharField(blank = True ,null = True, max_length=20) #optional

#     class Meta: 
#         ordering = ('-created',)
#         index_together = (('id', 'slug'),)
    
#     def __str__(self, **kwargs):
#         return self.name
    
#     def __str__(self):
#         return self.Product_SKU

#     # def get_absolute_url(self):
#     #     return reverse('shop:product_detail', args=[self.id, self.slug])

#     def get_absolute_url(self):
#         return reverse('shop:product_detail', args=[self.id, self.slug])

class CompanyName(models.Model):
    companyname_id = models.AutoField(primary_key=True)
    companyname = models.CharField(max_length=250, unique=True )
    
    

    def __str__(self):
        return self.companyname


class Company(models.Model): 
    Product_SKU = models.ForeignKey(Variant, related_name='prd_sku', on_delete=models.CASCADE)
    company_sku =  models.CharField(max_length=255, unique=True)
    company_name=models.ForeignKey(CompanyName, related_name='company_name', on_delete=models.CASCADE)
    # height = models.IntegerField(max_length=20)
    # width = models.IntegerField(max_length=20)
    # Company_Unit_Price= models.CharField(max_length=255)
    # Company_Retail_Price = models.CharField(max_length=255)
    # UPC/GTIN = models.IntegerField(max_length=20
    
    
    def __str__(self):
        return str(self.company_name)

    # def __str__(self):
    #    return self.company_name

class Manufacturer(models.Model):

    manufacturer_sku=models.CharField(max_length=250)
    manufacturer_name=models.CharField(max_length=250)
    manufacturer_product_Quantity =models.IntegerField(default=0)
    # Product_SKU = models.ForeignKey(Product, related_name='Manu_prd_sku')

    # def __str__(self):
    #     return (self.manufacturer_sku)

    


# def check_inventery(sender, **kwargs):#RECIVER
#     # if kwargs['created']:
#         #create_inventory= Order.objects.create(user=kwargs['instance'])
#     #create_inventory= Order.objects.create(user=kwargs['instance'])
#     quantity= Product.objects.all()
#     print quantity
#     print "message is recived"
