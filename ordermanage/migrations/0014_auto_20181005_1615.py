# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-10-05 10:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ordermanage', '0013_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='traking_number',
            field=models.CharField(blank=True, max_length=524, null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='invoice_send',
            field=models.CharField(choices=[('YES', 'YES'), ('NO', 'NO'), ('NA', 'NA')], default='NO', max_length=4, verbose_name='Invoice Raised'),
        ),
    ]
