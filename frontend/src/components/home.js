import React, { Component } from "react";
import {Container, Row, Col, Card, Form, Button } from "react-bootstrap";
import { withRouter } from "react-router";
import Sidebar from "./navar";
import './style/Dashboard.css'
import SidebarManufacturer from "./navbarMf";
import Topbar from "./topbar";
import Invcard from "./InvCard";
import ItemComtainer from "./ProductsContainer";
import { BrowserRouter } from "react-router-dom";

class Home extends Component{
    constructor(props){
        super(props)
        this.state={

        }
        
    }
    componentDidMount(){
        const token =  "Token "+this.props.location.state.token
        const requestOptions = {
            method: 'GET',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': token,
            },
            // body: JSON.stringify({data:this.props.location.state.token})    
            
        }
        
        fetch(process.env.URL+'api/', requestOptions)
            .then(response => response.json())
            .then(data => this.setState(data))
            .then(console.log(this.state))
    }
    render(){
        console.log(this.state)
        console.log(this.props.location.state.token)
        return(
            <div>
                <div>
                {this.state.Groups==='manufacturer'? <SidebarManufacturer user={this.state.username}/> : <Sidebar user={this.state.username}/> }
                </div>
                <div className="topbar">
                    <Topbar user={this.state.username}/>
                </div>
                <div>
                    <Invcard/>
                </div>
                <div>
                    <ItemComtainer token={this.props.location.state.token}/>
                    
                </div>
            </div>
        )
    }
}

export default Home