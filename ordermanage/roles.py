from rolepermissions.roles import AbstractUserRole

class IndianTeam(AbstractUserRole):
    available_permissions = {
        'create_po': True,
    }

class WareHouseTeam(AbstractUserRole):
    available_permissions = {
        'update_carrier_date': True,
        'warehouse_status': True,
        'create_remark': True,
    }


class SystemAdmin(AbstractUserRole):
    available_permissions = {
        'drop_tables': True,
    }