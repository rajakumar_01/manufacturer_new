from django.http import HttpResponse, HttpResponseRedirect,JsonResponse
from .Serializer import EmabadedAPP
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, Permission
from .forms import LoginForm, SignUpForm, AuthenticationForm
from django.contrib.auth.decorators import login_required
from rolepermissions.roles import assign_role
from django.views.decorators.csrf import csrf_exempt
# import shopify
from shopify_admin.models import ShopifyAccessToken
# from shopify_app.decorators import shop_login_required
from django.contrib.auth.models import Group
# from django.contrib.auth import get_user_model
'''==========================================================================
    Project Name: OrderManage
    Function Name: MVP Page
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def mvp(request):
    return render(request,"mvp.html", {})


'''==========================================================================
    Project Name: OrderManage
    Function Name: LogIn
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''


def user_login(request):
    print("hello")
    print(request.method)
    if request.method == 'POST':
        print("hello2")
        print(request.POST)
        form = LoginForm(request.POST)
        print(form.is_valid())
        if form.is_valid():
            cd = form.cleaned_data
            print(cd)
            user = authenticate(username=cd['username'], password=cd['password'])
            print(user)
            login(request, user)
            if user is not None:
                #if user.is_superuser:
                return HttpResponseRedirect('shop/')            
            #elif user.is_staff:
                #return HttpResponseRedirect('ordermanage/staff')

            else:
                return HttpResponse('Invalid login')
    
        
    # else:
    #     print('shop' in request.GET.keys())
        
        
    #     # return HttpResponseRedirect('shop/') 
    #     if(len(request.GET)==0):
    #         print('hello3')
    form = LoginForm()
    #     else:
    #         if('next' in request.GET):
                
    #             return HttpResponseRedirect('shop/')   
    #         else:
    #             print('hello4')
    #             username=ShopifyAccessToken.objects.get(shopify_shop= request.GET['shop']).user_name
    #             print(username)
    #             # user = User.objects.get(username=username)
    #             # print(user)
    #             user=authenticate(username='raj', password='123')
    #             print(user)
    #             login(request, user)
                
    #             print(user)
    #             return HttpResponseRedirect('shop/')  
    return render(request, 'mvp-2.html', {'form': form})
# @csrf_exempt
# def user_login(request):
#     pass
    # if request.method == 'POST' :
    #     form = LoginForm(request.POST)
    #     print form
    #     if form.is_valid():
    #         cd = form.cleaned_data
    #         user = authenticate(username=cd['username'], password=cd['password'])
    #         print user
    #         # login(request, user)
    #         if user is not None and  user.is_active :                
    #             login(request, user)
    #             # return redirect('ordermanage/')
    #         else:
    #             return redirect('mvp')
    #             # if user.is_superuser:
    #             #     return HttpResponseRedirect('/ordermanage/')            
    #         # elif user.is_staff:
    #         #     return HttpResponseRedirect('ordermanage/staff')

    #         # else:
    #         #     return HttpResponse('Invalid login')

    # else:
    #     form = LoginForm()
    # return render(request, 'mvp.html', {'form': form})
    # # return render_to_response('mvp.html', {'shouts' : shouts,
    #                                          'form' : form },
    #                           context_instance = RequestContext(request))


'''==========================================================================
    Project Name: OrderManage
    Function Name: LogOut
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''


# def user_logout(request):
#     # form = LoginForm()
#     return render(request, 'logout.html', {'section': 'logout'})

def user_logout(request):
    logout(request)
    request.session.flush()
    return redirect('mvp')
    
    # 404 page
# def errorpage(request):
#     return render(request,"404.html", {})   

# def signup(request):
#     if request.method == "POST":
#         form = SignUpForm(request.POST)
#         if form.is_valid():
#             user = form.save(commit=False)
#             user.save()
#             username = form.cleaned_data['username']
#             email = form.cleaned_data['email']
#             password = form.cleaned_data['password1']
#             User.objects.create_user(
#                 username=username, email=email, password=password)
#             # username = form.cleaned_data.get('username')
#             # raw_password = form.cleaned_data.get('password1')
#             # user = authenticate(username=username, password=raw_password)
#             # login(request, user)
#             return redirect('mvp',)
#     else:
#         form = SignUpForm()
#     return render(request, 'mvp.html', {'form': form})

'''==========================================================================
    Project Name: OrderManage
    Function Name: SignUp
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def signup(request):
    print('inform')
    if request.method == "POST":
        print('inpost')
        form = SignUpForm(request.POST)
        print(form)
        if form.is_valid():
            print('save')
            user=form.save()
            group = Group.objects.get(name=request.POST.get('Select_Group'))
            user.groups.add(group) 
            print (group)
            user.save()
            return redirect('mvp',)
    else:
        form = SignUpForm()
        print('not done')    
    return render(request, 'signup.html', {'form': form})



