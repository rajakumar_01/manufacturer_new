# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-09-07 11:55
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ordermanage', '0003_linetable'),
    ]

    operations = [
        migrations.AlterField(
            model_name='linetable',
            name='purchase_order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Order_purchase_order', to='ordermanage.Order'),
        ),
        migrations.AlterField(
            model_name='order',
            name='company_name',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Company_company_name', to='shop.Company'),
        ),
        migrations.AlterField(
            model_name='order',
            name='courier_method',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='CourierMethod_courier_method', to='ordermanage.CourierMethod'),
        ),
        migrations.AlterField(
            model_name='order',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='remark',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='User_username', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='sku_table',
            name='Po_request',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Order_po_order', to='ordermanage.Order'),
        ),
        migrations.AlterField(
            model_name='sku_table',
            name='company_name',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Company1_ompany_name', to='ordermanage.Company1'),
        ),
    ]
