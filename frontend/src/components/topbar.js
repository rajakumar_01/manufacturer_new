import React,{Component} from 'react'
import {Navbar} from 'react-bootstrap'
class Topbar extends Component{
    constructor(){
        super()
    }
    render(){
        return(
            <div>
            <Navbar variant="dark">
                
                
                <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text>
                    <h3>Signed in as: {this.props.user}</h3>
                    </Navbar.Text>
                </Navbar.Collapse>
            </Navbar>
            </div>
        )
    }
}

export default Topbar