from django.urls import path
from django.conf.urls.static import static
from . import views
app_name='shopify_admin'
urlpatterns = [
    # url(r'^$', views.ShopifyRequest, name='shopifyrequest'), # index page 
    # url(r'^response/$', views.ReciveOrder, name='response'), # recive order
    # url(r'^fullfill/$', views.OrderFullfill, name='fullfill'),
    # url(r'^inventory/$', views.CreateInventory, name='inventory'), # ctreate inventory
    # url(r'^create_product/$', views.CreateProduct, name='create_product'),
    # url(r'^token1/$', views.ShopifyLogin, name='token1'), # token page 
    # url(r'^catch/$', views.ShopifyRedirect, name='catch'), 
    # url(r'^orderresponse/$', views.ReciveOrderByToken, name='orderresponse'),
    # url(r'^createproduct/$', views.products, name='createproduct'),
    # url(r'^updateproduct/$', views.CreatePublicInventory, name='updateproduct'),
    # url(r'^orderfullfill/$', views.OrderPublicFullfill, name='orderfullfill'),
    # url(r'^allproducts/$', views.all_products, name='allproducts'),

    path('', views.ShopifyRequest, name='shopifyrequest'),
    path('token1/', views.ShopifyLogin, name='token1'),
    path('catch/',views.ShopifyRedirect, name='catch'),
    # path('orderresponse/',views.ReciveOrderByToken, name='orderresponse'),
    # path('createproduct/', views.products, name='createproduct'),
    # path('updateproduct/',views.CreatePublicInventory, name='updateproduct'),
    # path('orderfullfill/', views.OrderPublicFullfill, name='orderfullfill'),
    path('allproducts/',views.products,name='allproducts'),
    path('register/', views.register,name='register')

]