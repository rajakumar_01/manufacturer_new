from __future__ import unicode_literals
import django.core.handlers.wsgi  # Anuj not sure if this is used.
import datetime
from django.utils import timezone
from django.conf import settings  # Anuj not sure if this is used.
from django.urls import reverse
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from shop.models import *
# from wallmart.models import *
from django.forms import ModelChoiceField
from django.core import validators
from django.core.validators import RegexValidator
# import json, urllib3, shopify, requests
from shop.models import *

# These are generat status t0 be used across applications
# -----------------------------------------------

class GeneralStatus(models.Model):
    GENERAL_STATUS_CHOICES = (
        ('YES', 'YES'),
        ('NO', 'NO'),
        ('NA', 'NA'),
    )


class StatusChoice(models.Model):
    STATUS_CHOICES = (
        ('OPEN', 'OPEN'),
        ('CLOSE', 'CLOSE'),
        ('NA', 'NA'),
    )  # Status of the order


class PortalType(models.Model):
    PORTAL_CHOICES = (
        ('RETAIL', 'RETAIL'),
        ('ONINE', 'ONLINE'),
    )  # Status of the order


# -----------------------------------------------
# Done
# -----------------------------------------------
# Create your models here.
# seperate table for company Name
# shivam-  This is not company table which we are using 
class Company1(models.Model):
    company_id = models.AutoField(primary_key=True, verbose_name="Company ID")  
    company_name = models.CharField(max_length=500, 
                                    blank=True, 
                                    null=True, 
                                    verbose_name='Company Name',
                                    unique=True)
    Product_SKU = models.ForeignKey(Variant, 
                                    related_name='prduct_sku', on_delete=models.CASCADE)
    company_sku = models.CharField(max_length=255)

    def __str__(self):
        return (self.company_sku)

class CourierMethod(models.Model):
    courier_method = models.CharField(max_length=256, unique=True)
    valid_code = models.CharField(max_length=6, verbose_name="Service Code", unique=None)
    def __str__(self):
        return (self.courier_method)

class Order(models.Model): 
    # Removed for Ebay order id
    # alphanumeric            = RegexValidator(r'^[0-9a-zA-Z]*$',
    #                                             'Only alphanumeric characters are allowed.'), validators=[alphanumeric]
     
    user                    = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user')    
    po                      = models.CharField(max_length=50, primary_key=True)
    name                    = models.CharField(max_length=540)    
    # company_name            = models.ForeignKey(CompanyName, related_name='Company_company_name')
    company_name            = models.ForeignKey(Company, related_name='Company_company_name', on_delete=models.CASCADE) # change for company display
    courier_method          = models.ForeignKey(CourierMethod, related_name='CourierMethod_courier_method', on_delete=models.CASCADE) 
    Directory_Name          = models.CharField(max_length=256)  
    order_date              = models.DateField(auto_now_add=True, blank=False, null=False) 
    order_po_date           = models.DateField(default=datetime.date.today)  
    pickup_date             = models.DateField(blank=True, null=True)  
    carrier_date            = models.IntegerField(default=128, blank=True,
                                       null=True)  
    asn_sent                = models.CharField(max_length=128,
                                                choices=GeneralStatus.GENERAL_STATUS_CHOICES,
                                                default="NO", 
                                                blank=False, 
                                                null=False,
                                                verbose_name='ASN Sent?')  
    status                  = models.CharField(max_length=128, 
                                                choices=StatusChoice.STATUS_CHOICES,
                                                default="OPEN")
    warehouse_staff_status  = models.CharField(max_length=10, 
                                                verbose_name='Wharehouse Status',
                                                choices=StatusChoice.STATUS_CHOICES, 
                                                default="OPEN", 
                                                blank=False,
                                                null=False)
    invoice_send            = models.CharField(verbose_name='Invoice Raised', 
                                                choices=GeneralStatus.GENERAL_STATUS_CHOICES,
                                                default="NO", 
                                                max_length=4)
    remark                  = models.TextField(max_length=524, 
                                                blank=True, 
                                                null=True)
    Warehouse_remark        = models.TextField(max_length=524, 
                                                blank=True, 
                                                null=True)
    flag                    = models.IntegerField(blank=True, null=True)
    traking_number          = models.CharField(max_length=524, blank= True, null= True)

    def __str__(self):
        return (self.po)    
    def get_absolute_url(self):
        return reverse('order:index', args = [self.po])


################
# Remark table # 
################

class remark(models.Model):
    remark_po   = models.ForeignKey(Order, related_name = "remark1",verbose_name = 'Remark PO', on_delete=models.CASCADE)
    user        = models.ForeignKey(User, on_delete = models.CASCADE, related_name='User_username') # Anuj Please be consistent and use Related names everyehwere you are specifying a Foriegn Keys - TODO
    comment     = models.CharField(max_length = 500,verbose_name = 'Remark')    
    date        = models.DateTimeField(auto_now_add = True, auto_now=False) # Anuj - changed


'''   line table '''


class LineTable(models.Model):
    purchase_order  = models.ForeignKey(Order, related_name='Order_purchase_order', on_delete=models.CASCADE)   
    vendor_SKU      = models.CharField(max_length=256, blank=False,null=False)
    order_quantity  = models.IntegerField(default = 1,null = False, blank = False)

    def __str__(self):
        return str(self.order_quantity)

    # FOR getting absolute url
    def get_absolute_url(self):
        return reversed("ordermanage:edit", kwargs={"pk": self.purchase_order})


# for po sku
class Sku_Table(models.Model):    
    Po_request = models.ForeignKey(Order, related_name='Order_po_order', on_delete=models.CASCADE)
    Product_Sku = models.CharField(max_length=256, default='', null=False,blank=False)
    company_name = models.ForeignKey(Company1, related_name='Company1_ompany_name', on_delete=models.CASCADE)
    order_quantity = models.IntegerField(default='0')

    def __str__(self):
        return (self.Product_Sku)
#     # nothing this is used if we need to use some other dbtable  place of existing 
#     # class Meta: # Anuj - WHat is this field and why ?
#     #     db_table = "ordermanage"


class GetOrder(models.Model):
    customer_id = models.CharField(max_length =200)
    customer_name = models.CharField(max_length =200)
    contact_number = models.BigIntegerField( default=True, null=True, blank=True)

    def __str__(self):
        return self.customer_name

class ShopifyAccessToken(models.Model):
    shopify_shop = models.CharField(primary_key = True, max_length =200, null=False, blank=False)
    access_token = models.CharField( max_length =200, null=False, blank=False)
    scope = models.CharField(max_length =200, null=False, blank=False)

    def __str__(self):
        return self.access_token

class Address(models.Model):
    po_number = models.ForeignKey(Order, on_delete=models.CASCADE)
    customer_name = models.CharField(max_length=256, null=True)    
    address = models.CharField(max_length=256, null=True)
    city = models.CharField(max_length=256, null=True)
    state = models.CharField(max_length=256, null=True)
    zip = models.CharField(max_length=15, null=True)
    phone_number = models.CharField(max_length=20, null=True)

    def __str__(self):
        return str(self.po_number)





