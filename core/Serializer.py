from rest_framework import serializers
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from shop.models import Variant,Products
from shopify_admin.models import ShopifyAccessToken
class UserSerializer(serializers.Serializer):
    Groups = serializers.CharField(max_length=50)
    username = serializers.CharField(max_length=50)
    email = serializers.CharField(max_length=50)
    firstname = serializers.CharField(max_length=50)
    lastname = serializers.CharField(max_length=50)


class SignSerializer(serializers.Serializer):
    Groups = serializers.CharField(max_length=50)
    password1 = serializers.CharField(max_length=50)
    password2 = serializers.CharField(max_length=50)
    username = serializers.CharField(max_length=50)
    first_name = serializers.CharField(max_length=50)
    last_name = serializers.CharField(max_length=50)
    email = serializers.EmailField()
    def save(self, validated_data):
        group = Group.objects.get(name=validated_data['Groups'])
        
        user = User.objects.create_user(validated_data['username'],email= validated_data['email'],first_name=validated_data['first_name'],
                last_name=validated_data['last_name'],password=validated_data['password1'])
        print(user.__dict__)
        user.groups.set([group])
        return user

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = '__all__'
class VariantSerializer(serializers.ModelSerializer):
    product_id = serializers.SerializerMethodField()
    class Meta:
        model = Variant
        fields = '__all__'
    def get_product_id(self,obj):
        print(type(obj))
        return ProductSerializer(Products.objects.get(id=obj.product_id.id),read_only=True).data
class EmabadedAPP(serializers.Serializer):
    hmac=serializers.CharField(max_length=50)
    locale=serializers.CharField(max_length=50)
    session = serializers.CharField(max_length=50)
    shop = serializers.CharField(max_length=50)
    timstamp = serializers.CharField(max_length=50)
    
class ShopifyTokens(serializers.Serializer):
    # shopify_shop = serializers.PrimaryKeyRelatedField(read_only=True)
    # shopify_shop=serializers.CharField(max_length=50)
    user_name=serializers.CharField(max_length=50)
    access_token = serializers.CharField(max_length=50)
    scope = serializers.CharField(max_length=50)