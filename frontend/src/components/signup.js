import React, { Component } from 'react'
import { Container, Form , Col, Button} from 'react-bootstrap'
import logo from './media/logo.png'
import { Redirect } from 'react-router-dom'
import Login from './Login'
class Signup extends Component{
    constructor(){
        super()
        this.state={
            username:"",
            first_name:"",
            last_name:"",
            email:"",
            password1:"",
            password2:"",
            Groups:"",
            redirect:false,
            
        }
        this.onupdate = this.onupdate.bind(this)
        this.onsubmit = this.onsubmit.bind(this)
    }
    onupdate(event){
        this.setState({[event.target.name]:event.target.value})
        console.log(this.state)
    }
    onsubmit(){
        const requestOptions = {
            method: 'post',
            headers: { 
                'Content-Type': 'application/json',
                
            },
            body: JSON.stringify(this.state)
        }
        fetch(process.env.URL+'api/signup/', requestOptions)
            .then(response => response.json())
            .then(this.setState({redirect:true}))
        
    }
    render(){
        console.log('login')
        if(this.state.redirect){
            return(
                <Redirect to={''}/>
            )
        }
        return(
            <div>
                <Container style={{marginTop:50,textAlign:"center"}}>
                    <img src={ logo } alt="image"/>
                    
                    <h2 style={{marginTop:50}}>Please, login into your account</h2>
                    
                    <Container>
                    <Form>
                        <Form.Row>
                            <Col>
                                <Form.Label>Username</Form.Label>
                                <Form.Control placeholder="Username" name="username" onChange={this.onupdate}/>
                            </Col>
                            <Col>
                                <Form.Label>Firstname</Form.Label>
                                <Form.Control placeholder="Firstname" name="first_name"  onChange={this.onupdate}/>
                            </Col>
                        </Form.Row>
                        <Form.Row>
                            <Col>
                                <Form.Label>Lastname</Form.Label>
                                <Form.Control placeholder="Lastname" name="last_name"  onChange={this.onupdate}/>
                            </Col>
                            <Form.Group as={Col} controlId="formGridEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" placeholder="Enter email" name="email"  onChange={this.onupdate}/>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridPassword">
                                <Form.Label muted>Password</Form.Label>
                                <Form.Control type="password" placeholder="Password" name="password1"  onChange={this.onupdate}/>
                            </Form.Group>
                            <Form.Group as={Col}>
                                <Form.Label>ConfirmPassword</Form.Label>
                                <Form.Control type="password" placeholder="Confirm Password" name="password2" onChange={this.onupdate}/>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                        
                        <Col md={{span:3,offset:3}}>
                            <Form.Check
                                type="radio"
                                label="manufacturer"
                                name='Groups'
                                value='manufacturer'
                                onChange={this.onupdate}
                            />
                        </Col>
                        <Col>
                            <Form.Check
                                type="radio"
                                label="warehouse"
                                name='Groups'
                                value='warehouse'
                                onChange={this.onupdate}
                            />
                        </Col>
                        </Form.Row>
                        <Button type='button' onClick={this.onsubmit}>Signup</Button>         
                    </Form>
                </Container>
                </Container>
            </div>
        )
    }
}

export default Signup