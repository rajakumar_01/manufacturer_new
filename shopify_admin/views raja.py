
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse
from .models import *
# from .models import *
from shop.models import Product,Category,Company,CompanyName,Manufacturer
import json, requests

def ShopifyRequest(request):
    return render(request, 'shopify_app/index.html', {})

def ShopifyLogin(request):
    api_key = '9d2f128f479705d229ad06bce043165e'
    api_secret_key = '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'
    # api_key = 'ead441fdcf80b836985b08647f6119c0'
    # api_secret_key= '8e243f0dd108f7402056831f5d72c22d'
    scope =str(['read_products'])    
    response = "https://ordermanaged.myshopify.com/admin/oauth/authorize?client_id="+api_key +'&scope=write_products,read_orders,write_orders' +'&redirect_uri='+'http://127.0.0.1:8000/shopify_app/catch'+'&state=12345'
    return HttpResponseRedirect(response)

def ShopifyRedirect(request):
    api_key = '9d2f128f479705d229ad06bce043165e'
    api_secret_key = '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'
    getresponse  = request.GET.get('shop')
    code= str(request.GET.get('code'))   
    timestamp= request.GET.get('timestamp')
    hmac = request.GET.get('hmac')
    headers  = {'Content-type':'application/json'}
    datas = {        
        'client_id':api_key,
        'client_secret':api_secret_key,
        'code':code,
        }
    url =requests.post('https://ordermanaged.myshopify.com/admin/oauth/access_token', data=datas)
    print(url.content)
    a=  json.loads(url.content)
    query_accesstoken = ShopifyAccessToken
    if a:
        instance = query_accesstoken(shopify_shop=getresponse ,access_token =a['access_token'], scope = a['scope'] )
        instance.save()    
    return redirect('shopifyrequest')

"""===========================================================
===================recive Order with token key ==============
============================================================"""
def ReciveOrderByToken(request): 
    access_token = ShopifyAccessToken.objects.get(shopify_shop= "ordermanaged.myshopify.com")  
    print (access_token.access_token)
    if request.method == 'GET':
        headers ={
            'Content-Type':'application/json',
            'X-Shopify-Access-Token':access_token.access_token,       
        }
        shop_url = "https://ordermanaged.myshopify.com/admin/orders.json"        
        response = requests.get(shop_url , headers = headers )
        data = json.loads(response.content)       
        order_request = GetOrder
        customer_address = Address        
        shopify_order = Order       # Order(main table )Objects
        company = (Company.objects.filter(company_name=CompanyName.objects.get(companyname="Shopify").companyname_id))[0]     
        courier = CourierMethod.objects.get(courier_method="UPS Ground")
        shopi_order_qty = LineTable      
        for order in data["orders"]:    
            po =order["id"]
            dd= order["created_at"]
            dateOrd = str(dd).split("T")
            orderdate= datetime.datetime.strptime(dateOrd[0], '%Y-%m-%d')
            id = order['customer']['id']
            name = order['customer']['first_name']
            contact_number =order['customer']['phone']
            shopify_recive_order = shopify_order(po=str(po) ,user =User.objects.get(id=request.user.id) ,company_name=company ,courier_method=courier, order_date= orderdate)
            shopify_recive_order.save()            
            order_recive = order_request(customer_id=id ,customer_name= name, contact_number =contact_number  )
            order_recive.save()
            shopify_lineItem =shopi_order_qty(purchase_order =Order.objects.get(po=po), vendor_SKU = order['line_items'][0]['sku'],order_quantity =order['line_items'][0]['fulfillable_quantity']  )
            shopify_lineItem.save()
            phone = ""
            if order['phone']:
                phone = order['phone']
            else:
                phone = order['email']
            customer_address_query = customer_address(po_number=Order.objects.get(po=po),
                                     customer_name=order['customer']['default_address']['name'],
                                     city =order['customer']['default_address']['city'],
                                     address =(order['customer']['default_address']['name']                        
                                     +', ' +order['customer']['default_address']['address1']
                                     +', ' +order['customer']['default_address']['address2']
                                     +', ' +order['customer']['default_address']['city']
                                     + ', ' +order['customer']['state']
                                     + ', ' +order['customer']['default_address']['country'] ),
                                     state = order['customer']['state'],
                                     zip =order['customer']['default_address']['zip'],
                                    phone_number =phone,                                     
                                    )
            customer_address_query.save()
        return redirect('index')


def products(request):
    access_token = ShopifyAccessToken.objects.get(shopify_shop= "ordermanaged.myshopify.com")  
    print (access_token.access_token)
    headers ={
        'Content-Type':'application/json',
        'X-Shopify-Access-Token':access_token.access_token,       
    }
    datas = {
                "product": {    
                                
                                "title": "autkey1",
                                "body_html": "<strong>Good snowboard!</strong>",
                                "vendor": "ordermanaged",
                                "product_type": "ordermanaged",
                                "tags": "bedsheets, Bedsheets;"
                            }
                }
    response= requests.post("https://ordermanaged.myshopify.com/admin/products.json", data=json.dumps(datas), headers=headers)    
    print (response)
    return HttpResponse(response)


"""====================================================
create Invetory With access Token 
====================================================="""


def CreatePublicInventory(request):
    access_token = ShopifyAccessToken.objects.get(shopify_shop= "ordermanaged.myshopify.com")

    headers ={
        'Content-Type':'application/json',
        'X-Shopify-Access-Token':access_token.access_token,       
    }
    # shop_url = "https://ordermanaged.myshopify.com/admin/inventory_items.json"
    datas = {
                "variant": { 
                                    'price':'252.00',   
                                    'option1': 'yellow'
                                  }
            }                                                                   #adding product id with the url to add inventory varient
    response = requests.post("https://ordermanaged.myshopify.com/admin/products/4557537181749/variants.json" , data= json.dumps(datas), headers =headers,)
    print(response)
    return HttpResponse(response)

"""====================================================
Order Fullfillment With access Token 
====================================================="""

def OrderPublicFullfill(request):
    access_token = ShopifyAccessToken.objects.get(shopify_shop= "ordermanaged.myshopify.com")
    headers ={
        'Content-Type':'application/json',
        'X-Shopify-Access-Token':access_token.access_token,       
    }
    query_set = Order.objects.all().filter(company_name__company_name__companyname='Shopify')
    for i in Order.objects.all():
        print(i.company_name.company_name.companyname)
        print(type(i.company_name.company_name.companyname))
    print(query_set)
    for i in query_set:
        po = i.po
        print (po       )
    datas = {
                "fulfillment": {
                    "order_id":po,                    
            }
        }   
    response = requests.post('https://ordermanaged.myshopify.com/admin/orders/'+po+ '/fulfillments.json', headers =headers, data= json.dumps(datas))
    return HttpResponse(response)

'''==========================================
   ==============UPS INtegration ============
   =========================================='''
def all_products(request):
    access_token = ShopifyAccessToken.objects.get(shopify_shop= "ordermanaged.myshopify.com")
    headers ={
        'Content-Type':'application/json',
        'X-Shopify-Access-Token':access_token.access_token,       
    }
    response=requests.get("https://ordermanaged.myshopify.com/admin/products.json",headers= headers)
    print(response)
    data=response.json()
    # print(data)
    for key in data['products']:
        productid= key['id']
        optionid= key['options'][0]['id']
        for variant in key['variants']:
            print(variant)
            variantid= variant['id']
        
            try:
                imgid=variant['id']
            except IndexError:
                imgid= 'none'
            # shopifyproductsave = ShopifyProductDetails(product_sku= productid, product_id= productid, variants_id= variantid, options_id= optionid, images_id= imgid)
            # shopifyproductsave.save()
            product=Product(Product_SKU=variantid,
                        name= key['title'],
                        slug= key['title'],
                        image=key['image'],
                        description=key['body_html'],
                        bulletpoints="",
                        category=key['product_type'],
                        product_Dimensions=variant['weight'],
                        color=variant['title'],
                        price=variant['price'],
                        quantity=variant['sku'],
                        block_quantity=variant['inventory_quantity'],
                        available_quentity=variant['inventory_quantity'],
                        created=variant['created_at'],
                        available=variant['inventory_quantity'],
                        updated=variant['updated_at'],
                        Material_Type=key['product_type'],
                        Model_Number=key['id'],
                        ASIN="")
            product.save()    

    return HttpResponse(response)



